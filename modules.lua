-- MODULES
-- * mandatory include-config file for all technolgies/modules refencing any notAlab submodule with other submodules dependencies
-- * ALTERNATIVE DESCRIPTION: here you just fill paths for 3rd and higher levels potential dependencies

local MODULES_DIR = "modules/"

modules = {
	-- single files
	message = {
		data = {
			path = MODULES_DIR .. "core/api/message/",
			head = "message.lua",	
		},
	},
	cmdDesc = {
		data = {
			path = MODULES_DIR .. "core/api/cmdDesc/",
			head = "cmdDesc.lua",	
		},
	},
	mathExt = {
		data = {
			path = MODULES_DIR .. "core/ext/mathExt/",
			head = "mathExt.lua",	
		},
	},
	stringExt = {
		data = {
			path = MODULES_DIR .. "core/ext/stringExt/",
			head = "stringExt.lua",
		},
	},
	tableExt = {
		data = {
			path = MODULES_DIR .. "core/ext/tableExt/",
			head = "tableExt.lua",
		},
	},
	timeExt = {
		data = {
			path = MODULES_DIR .. "core/ext/timeExt/",
			head = "timeExt.lua",	
		},
	},
	vec3 = {
		data = {
			path = MODULES_DIR .. "core/vec3/",
			head = "vec3.lua",	
		},
	},
	hmsf = {
		data = {
			path = MODULES_DIR .. "core/hmsf/",
			head = "hmsf.lua",	
		},
	},
	json = {
		data = {
			path = MODULES_DIR .. "core/json/",
			head = "json.lua",	
		},
	},
	actionTypes = {
		data = {
			path = MODULES_DIR .. "core/tsp/actionTypes/",
			head = "actionTypes.lua",
		},
	},
	attach = {
		data = {
			path = MODULES_DIR .. "core/mod/attach/",
			head = "attach.lua",
		},
	},
	poi = {
		data = {
			path = MODULES_DIR .. "core/poi/",
			head = "poi.lua",
		},
	},
	
	-- more files
	customCommands = {
		config = {
			path = MODULES_DIR .. "customCommands/config/",
			files = {},
		},
		data = {
			path = MODULES_DIR .. "customCommands/data/",
			head = "init.lua",	
		},	
	},
	goals = {
        config = {
            path = MODULES_DIR .. "goals/config/",
			files = {
				"missionGoals.lua"
			}
        },
		data = {
            path = MODULES_DIR .. "goals/data/",
            head = "init.lua"
        }
    },
	notAchili = {
		config = {
			notaUI = {
				path = MODULES_DIR .. "notAchili/notaUI/",
				files = {
					"tools.lua",
					"unitControlTools.lua",
					"minimapWidget.lua",
					"selectionWidget.lua",
					"ordersWidget.lua",
					"buildWidget.lua",
					"resourceBarWidget.lua",
					"consoleWidget.lua",
					"missionGoalsWidget.lua",
				},
			},
		},
		data = {
			path = MODULES_DIR .. "notAchili/data/",
			head = "core.lua",	
		},	
	},
	resources = {
		config = {
			path = MODULES_DIR .. "resources/config/",
			files = {
				"resourcesConstants.lua",
				"resourcesTypes.lua",
				"resourcesModes.lua",
			},
		},
		data = {
			path = MODULES_DIR .. "resources/data/",
			head = "init.lua",	
		},	
	},
	spawn = {
		config = {
			path = MODULES_DIR .. "spawn/config/",
			files = {
				"basicObjects.lua",
				"basicProjectiles.lua",
				"notaLegacyObjects.lua",
			},
		},
		data = {
			path = MODULES_DIR .. "spawn/data/",
			head = "init.lua",	
		},	
	},
	c4 = {
		config = {
			path = MODULES_DIR .. "c4/config/",
			files = {},
		},
		data = {
			path = MODULES_DIR .. "c4/data/",
			head = "init.lua",	
		},	
	},
	
}