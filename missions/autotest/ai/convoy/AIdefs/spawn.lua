----- mission spawn list ------
----- more about: http://springrts.com/phpbb/viewtopic.php?f=55&t=28259

-- !! not finished --
-- ! not own spawner
-- ! need add resources start setting

newSpawnDef = {

}

newSpawnThis = {

}

local counter 	= 0
local limit		= 30
local line		= 1
local squareDist= 250

for id,unitDef in pairs(UnitDefs) do
	counter = counter + 1
	local row = counter % limit
	if (row == 0) then line = line + 1 end
	
	local uName	= unitDef.name
	local tName	= "t_" .. uName
	newSpawnDef[tName] = {unit = uName, class = "single"}
	-- newSpawnThis[counter] = {name = tName, posX = row*squareDist, posZ = line*squareDist, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
end

-- for i=1, 10 do
	-- newSpawnThis[#newSpawnThis+1] = {name = "t_armrock", posX = 2400 + i*30, posZ = 2100, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	-- newSpawnThis[#newSpawnThis+1] = {name = "t_armham", posX = 2450 + i*30, posZ = 2200, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	-- newSpawnThis[#newSpawnThis+1] = {name = "t_armrock", posX = 2500 + i*30, posZ = 2300, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	-- newSpawnThis[#newSpawnThis+1] = {name = "t_armham", posX = 2550 + i*30, posZ = 2400, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
-- end

for i=1, 10 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armstump", posX = 3200 + i*30, posZ = 2500, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
end

for i=1, 8 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armsam", posX = 3400 + i*80, posZ = 2300, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
end

newSpawnThis[#newSpawnThis+1] = {name = "t_armrad", posX = 3600, posZ = 2100, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}

for i=1, 10 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3500 + i*30, posZ = 3000, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3500 + i*30, posZ = 3100, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3500 + i*30, posZ = 3200, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3500 + i*30, posZ = 3300, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
end

for i=1, 5 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armfark", posX = 3200 + i*20, posZ = 3500, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
end

newSpawnThis[#newSpawnThis+1] = {name = "t_armbase", posX = 1200, posZ = 4200, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}

for i=1, 5 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3200 + i*20, posZ = 5750, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3200 + i*20, posZ = 5850, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armpw", posX = 3200 + i*20, posZ = 5950, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
end

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armbull", posX = 4000 + i*30, posZ = 4900, facing = "e", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armbull", posX = 4000 + i*30, posZ = 5000, facing = "e", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armbull", posX = 4000 + i*30, posZ = 5100, facing = "e", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

-- for i=1, 8 do
	-- newSpawnThis[#newSpawnThis+1] = {name = "t_armhell", posX = 2000 + i*10, posZ = 2970, facing = "n", teamName = "player1", checkType = "none", gameTime = 0}
-- end

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 3000 + i*50, posZ = 250, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 3000 + i*50, posZ = 300, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 3000 + i*50, posZ = 350, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 2200 + i*50, posZ = 400, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 2200 + i*50, posZ = 450, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 2200 + i*50, posZ = 500, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

newSpawnThis[#newSpawnThis+1] = {name = "t_corfus", posX = 2550, posZ = 400, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] = {name = "t_armmex", posX = 2969, posZ = 766, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] = {name = "t_armmex", posX = 3690, posZ = 390, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] = {name = "t_armmex", posX = 2969, posZ = 766, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 3200 + i*50, posZ = 200, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 3200 + i*50, posZ = 250, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_armwin", posX = 3200 + i*50, posZ = 300, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

newSpawnThis[#newSpawnThis+1] = {name = "t_armhlt", posX = 2900, posZ = 800, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] = {name = "t_armhlt", posX = 3100, posZ = 750, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}

-- features
-- newSpawnThis[#newSpawnThis+1] 	= {name = "t_containermetal", posX = 3800, posZ = 1000, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}

-- real enemy
for i=1, 5 do
	newSpawnThis[#newSpawnThis+1] = {name = "t_corak", posX = 6500 + i*40, posZ = 1800, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_corak", posX = 5000 + i*30, posZ = 2500, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_corak", posX = 7500 + i*80, posZ = 3200, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] = {name = "t_corak", posX = 5500 + i*30, posZ = 4800, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end


-- technical enemy
newSpawnThis[#newSpawnThis+1] = {name = "t_pireye", posX = 8000, posZ = 8000, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] = {name = "t_corfus", posX = 7500, posZ = 7500, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] = {name = "t_coreter", posX = 7700, posZ = 7700, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}

function NewSpawner()
    --Spring.Echo("N.O.E. mission_spawner: mission spawner works, but its empty")
end