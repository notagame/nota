----- mission spawn list ------
----- more about: http://springrts.com/phpbb/viewtopic.php?f=55&t=28259

-- !! not finished --
-- ! not own spawner
-- ! need add resources start setting

newSpawnDef = {

}

newSpawnThis = {

}

local counter 	= 0
local limit		= 30
local line		= 1
local squareDist= 250

for id,unitDef in pairs(UnitDefs) do
	counter 				= counter + 1
	local row				= counter % limit
	if (row == 0) then line = line + 1 end
	
	local uName 			= unitDef.name
	local tName				= "t_" .. uName
	newSpawnDef[tName] 		= {unit = uName, class = "single"}
	-- newSpawnThis[counter] 	= {name = tName, posX = row*squareDist, posZ = line*squareDist, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
end

for i=1, 10 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armrock", posX = 4400  + i*30, posZ = 1050, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armham", posX = 4500 + i*30, posZ = 1100, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armrock", posX = 4400 + i*30, posZ = 1150, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armham", posX = 4500   + i*30, posZ = 1200, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
end

for i=1, 10 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armstump", posX = 3800 + i*30, posZ = 1600 , facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
end


for i=1, 12 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armmart", posX = 4500 + i*10, posZ = 800, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
end


for i=1, 8 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armhell", posX = 5000 + i*10, posZ = 800, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}
end

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 5600 + i*50 + math.random()*100, posZ = 2800 + math.random()*100, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 5700 + i*50 + math.random()*100, posZ = 2700 + math.random()*100, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 5800 + i*50 + math.random()*100, posZ = 2900 + math.random()*100, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 2200 + i*50, posZ = 400, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 2200 + i*50, posZ = 450, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 2200 + i*50, posZ = 500, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

newSpawnThis[#newSpawnThis+1] 	= {name = "t_corfus", posX = 5416, posZ = 3070, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armmex", posX = 4422, posZ = 2476, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armmex", posX = 3690, posZ = 390, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armmex", posX = 2969, posZ = 766, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}

for i=1, 3 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 3200 + i*50, posZ = 200, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 3200 + i*50, posZ = 250, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 3200 + i*50, posZ = 300, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

for i=1, 2 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_corcrabe", posX = 4344 + i*50, posZ = 2450, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_corcrabe", posX = 4444 + i*50, posZ = 2350, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_corcrabe", posX = 4544 + i*50, posZ = 2550, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end

newSpawnThis[#newSpawnThis+1] 	= {name = "t_armwin", posX = 4379, posZ = 2896, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armfff", posX = 5049, posZ = 2830, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armhklab", posX = 4428, posZ = 2971, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corllt", posX = 4456, posZ = 2256, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corllt", posX = 4946, posZ = 2121, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corllt", posX = 4471, posZ = 2971, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corhlt", posX = 4861, posZ = 2422, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corhlt", posX = 4358, posZ = 2632, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corhlt", posX = 4571, posZ = 2257, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corhrk", posX = 4500, posZ = 2350, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_corhrk", posX = 4600, posZ = 2350, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
	
for i=1, 10 do
	newSpawnThis[#newSpawnThis+1] 	= {name = "t_cordrag", posX = 4739 + i*4, posZ = 2189 - i * 10 , facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
end
	
	
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armhlt", posX = 3000, posZ = 1000, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_armhlt", posX = 3200, posZ = 1000, facing = "n", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}

-- features
-- newSpawnThis[#newSpawnThis+1] 	= {name = "t_containermetal", posX = 3800, posZ = 1000, facing = "s", teamName = "player1", checkType = "none", gameTime = 0}

-- technical enemy
newSpawnThis[#newSpawnThis+1] 	= {name = "t_pireye", posX = 8000, posZ = 8000, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
--newSpawnThis[#newSpawnThis+1] 	= {name = "t_corfus", posX = 7500, posZ = 7500, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}
newSpawnThis[#newSpawnThis+1] 	= {name = "t_coreter", posX = 7700, posZ = 7700, facing = "s", teamName = "defaultMissionAI", checkType = "none", gameTime = 0}


function NewSpawner()
    --Spring.Echo("N.O.E. mission_spawner: mission spawner works, but its empty")
end