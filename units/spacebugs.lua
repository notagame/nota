local spacebugs = {}
local queensHealth = {
	--cormonstaq = 100000, -- legacy
	cormonstaqeasy = 100000,
	cormonstaqmedium = 140000,
	cormonstaqhard = 180000,
	cormonstaqinsane = 160000,
}
local queensSize = {
	--cormonstaq = 1.0, -- legacy
	cormonstaqeasy = 1.0,
	cormonstaqmedium = 1.25,
	cormonstaqhard = 1.5,
	cormonstaqinsane = 1.75,
}
local queenWeaponsTemplate = {
	"fast_bugplasmaq",
	"monsta_missileq",
	"high_bugplasmaq",
	"high_bugasteroidbeacon",
	"queencrush",
}
local queenWeapons = {
	--cormonstaq = queenWeaponsTemplate, -- legacy
	cormonstaqeasy = queenWeaponsTemplate,
	cormonstaqmedium = queenWeaponsTemplate,
	cormonstaqhard = queenWeaponsTemplate,
	cormonstaqinsane = {
		"fast_bugplasmaq",
		"monsta_missileq",
		"high_bugplasmaq",
		"high_bugasteroidbeacon",
		"queencrush",
		"queenshield",
	},
}
	-- weapons = {
		-- [1] = {
			-- def = "fast_bugplasmaq",
			-- badTargetCategory = "VTOL",
			-- onlyTargetCategory = "NOTAIR",
			-- noChaseCategory = "VTOL",
		-- },
		-- [2] = {
			-- def = "monsta_missileq",
			-- badTargetCategory = "VTOL",
			-- onlyTargetCategory = "VTOL",
			-- noChaseCategory = "VTOL",
		-- },
		-- [3] = {
			-- def = "high_bugplasmaq",
			-- badTargetCategory = "VTOL",
			-- noChaseCategory = "VTOL",
		-- },
		-- [4] = {
			-- def = "high_bugasteroidbeacon",
			-- badTargetCategory = "VTOL",
			-- noChaseCategory = "VTOL",
		-- },
		-- [5] = {
			-- def = "queencrush",
			-- badTargetCategory = "VTOL",
			-- noChaseCategory = "VTOL",
		-- },
		-- [6] = {
			-- def = "queenshield",
		-- },
	-- },

local queenTemplate = {
	buildPic = "cormonstaq.pcx",
	objectName = "CORMONSTAQ",
	category = "NOTAIR NOTSUB BUG";
    name = "Montros Queen",
    side = "BUG",
    script = "cormonstaq.cob",
	
	buildTime = 507561,
	buildCostEnergy = 1400415,
	buildCostMetal = 120874,
	energyUse = 0.7,
	energyStorage = 10000,
	energyMake = 200,
    description = "Biomechanoid Assault Spider",
	icontype = "pod";
	mass = 45000,
    radarDistance = 4000,
    sightDistance = 800,
	idletime = 50,
	idleautoheal = 300,
	
	footprintX = 6,
	footprintZ = 6,
	brakeRate = 0.352587891,
	acceleration = 0.152587891,
	maxVelocity	= 3,
	maxSlope = 40,
    maxWaterDepth = 12,
    movementClass = "MONTRO",
    turnRate = 500,
	
	builder	= 0,
    canAttack = 1,
    canGuard = 1,
    canMove = 1,
    canPatrol = 1,
    canStop = 1,
    reclaimable = 0,
	onoffable = 1,
	
	collisionVolumeType = "cylY",
	collisionVolumeScales = {140, 125, 140},
    
	soundCategory = "KROGOTH",
	explodeAs = "MONSTADIE",
	
	highTrajectory = 2,
	
	badTargetCategory = "VTOL",
	noChaseCategory = "VTOL",		
	onlyTargetCategory1 = "NOTAIR",
	onlyTargetCategory2 = "VTOL",
	noChaseCategory2 = "NOTAIR",
	badTargetCategory2 = "NOTAIR",
}
for queenKey, healthValue in pairs(queensHealth) do
	local size = queensSize[queenKey]
	local health = healthValue
	local weapons = queenWeapons[queenKey]
	local definition = {}

	definition.maxdamage = health
	definition.weapons = weapons
	
	for k,v in pairs(queenTemplate) do
		definition[k] = v
	end

	spacebugs[queenKey] = definition
end

spacebugs["bug3r"] = {
-- Internal settings
    BuildPic = "filename.bmp",
    ObjectName = "spacebugs/bug3r.s3o",
	Category="NOTAIR NOTSUB BUG";
    name = "Heroic bug",
    Side = "BUG",
    UnitName = defName,
    --script = "bug.lua",
    
-- Unit limitations and properties
    BuildTime = 1000,
    Description = "Special kind of bug",
    MaxDamage = 800,
    RadarDistance = 0,
    SightDistance = 400,
    SoundCategory = "CORSCORP",
    Upright = 0,
	ExplodeAs = "ULTRABUG_DIE",
    
-- Energy and metal related
    BuildCostEnergy = 100,
    BuildCostMetal = 0,
   
-- Pathfinding and related
	MaxVelocity	= 5,
	BrakeRate = 0.4,
	Acceleration = 0.5,
    FootprintX = 2,
    FootprintZ = 2,
    MaxSlope = 30,
    MaxWaterDepth = 20,
    MovementClass = "BUGLARGEHOVER",
    TurnRate = 900,
    
-- Abilities
    Builder = 0,
    CanAttack = 1,
    CanGuard = 1,
    CanMove = 1,
    CanPatrol = 1,
    CanStop = 1,
    LeaveTracks = 0,
    Reclaimable = 0,
    
-- Hitbox
-- collisionVolumeOffsets    =  "0 0 0",
-- collisionVolumeScales     =  "20 20 20",
-- collisionVolumeTest       =  1,
-- collisionVolumeType       =  "box",

-- Weapons and related
	weapons = {
		[1] = {
			def = "ULTRA_MELEE",
			BadTargetCategory = "NOTAIR",
			ExplodeAs = "TANKDEATH",
			NoChaseCategory = "AIR",
		},
	},
}

spacebugs["bugtower"] = {
-- Internal settings
    BuildPic = "filename.bmp",
    Category = "NOTAIR NOTSUB NOTSHIP",
    ObjectName = "spacebugs/bugtower.s3o",
    name = "Bug spore tower",
    Side = "REP",
    UnitName = defName,
    script = "bugtower.lua",
	
-- visual
	useBuildingGroundDecal = true,
	buildingGroundDecalDecaySpeed = 0.01,
	buildingGroundDecalSizeX = 8,
	buildingGroundDecalSizeY = 8,
	buildingGroundDecalType = "buglair1s.png", 
	ExplodeAs = "BUGDIE",
	SelfDestructAs = "BUGDIE",
    
-- Unit limitations and properties
    BuildTime = 1000,
    Description = "Defence of hive",
    MaxDamage = 5*800,
    RadarDistance = 0,
    SightDistance = 400,
    SoundCategory = "LLT",
    Upright = 0,
	iconType = "tower",
    
-- Energy and metal related
    BuildCostEnergy = 100,
    BuildCostMetal = 0,
    
-- Abilities
    Builder	= 0,
    CanAttack = 1,
    CanGuard = 0,
    CanMove = 0,
    CanPatrol = 0,
    CanStop = 1,
    LeaveTracks = 0,
    Reclaimable = 0,
	stealth = 1,
    
-- Hitbox
	collisionVolumeOffsets = "0 15 0",
	collisionVolumeScales = "60 90 60",
	collisionVolumeTest = 1,
	collisionVolumeType = "cylY",
    
-- Weapons and related
	weapons = {},
	SFXTypes = {
		explosiongenerators = {
			"custom:ACIDSWARMSTATIONARY_Expl",
		},
	},
}

return lowerkeys(spacebugs)
