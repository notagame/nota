local SPEED_SLOW = 5
local SPEED_HIGH = 12
local currentSpeed = SPEED_SLOW

local function SmokeEffect(count)
	local smokecloud = SFX.CEG
	local bottom = piece "tube_botto"
	local top = piece "tube_top"
	local hose = piece "hose"
	local counter = 0
	if count == nil then count = -1 end -- forever
	
	Move(bottom, y_axis, -20, currentSpeed*4)
	Move(top, y_axis, -15, currentSpeed*4)
	Move(hose, y_axis, -15, currentSpeed*4)
	WaitForMove(bottom, y_axis) 
	while (count ~= counter) do		
		Move(bottom, y_axis, 0, currentSpeed*16)
		Move(top, y_axis, 0, currentSpeed*16)
		Move(hose, y_axis, 0, currentSpeed*16)
		WaitForMove(bottom, y_axis) 
		EmitSfx(hose, smokecloud)
		Sleep(4200-currentSpeed*300)
		counter = counter + 1
		Move(bottom, y_axis, -20, currentSpeed*4)
		Move(top, y_axis, -15, currentSpeed*4)
		Move(hose, y_axis, -15, currentSpeed*4)
		WaitForMove(bottom, y_axis)
		if currentSpeed > SPEED_SLOW then
			currentSpeed = currentSpeed - 1
		end
	end
	return 0
end

function script.Create()
	SmokeEffect()
    return 0
end

function script.HitByWeapon(x, z, weaponDefID, damage) 
	currentSpeed = SPEED_HIGH
end

function script.Killed(recentDamage, maxHealth)
	return 0
end