-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
local tableExt = attach.Module(modules, "tableExt")

IDLE_AUTO_HEAL = 10
IDLE_TIME = 600
MASS_LIGHT_BOT_TORSO = 200
MASS_HEAVY_BOT_TORSO = 800

SOLDIER = {
	light = {
		mass = MASS_LIGHT_BOT_TORSO,
		sightDistance = 600,
		seismicDistance = 1000,
		maxVelocity = 1.15,
		turnRate = 1000,
		acceleration = 0.1,
		brakeRate = 0.5,
		maxWaterDepth = 22,
	},
}

VELOCITY_SMG = 400
VELOCITY_MG = 900
VELOCITY_RIFLE = 1200

HEALTH_RIFLEMAN = 300
HEALTH_SNIPER = 320 -- armsnipe
HEALTH_SMG = 377 -- pw

RANGE_SMG = 285 -- pw emg
RANGE_SMG_LASER = 295 -- corak
RANGE_RIFLE = 500
RANGE_RIFLE_SNIPER = 650 -- armsnipe
RANGE_MG = 700

RIFLE_BASE = {
	weaponType = "LaserCannon",
	range = RANGE_RIFLE,
	reloadTime = 5,
	weaponVelocity = VELOCITY_RIFLE,	
	burst = 1,
	
	rgbColor = {1, 0.059, 0},
	rgbColor2 = {1, 1, 1},
}

RIFLE_SNIPER = {
	range = RANGE_RIFLE_SNIPER,
	reloadTime = 15, -- armsnipe
	
	rgbColor = {1.0, 0.2, 0.2}, -- armsnipe
}

SMG = {
	weaponType = "Cannon",
	range = RANGE_SMG,
	reloadTime = 0.4, -- pw emg
	burst = 4, -- pw emg
	burstrate = 0.1, -- pw emg
	weaponVelocity = VELOCITY_SMG,  -- pw emg
	sprayangle = 768, -- pw emg
	areaofeffect = 8, -- pw emg
	
	lineofsight = 1, -- pw emg
	
	rgbcolor = {1, 0.93, 0.45},  -- pw emg
	intensity = 0.9, -- pw emg
}

MG = {
	weaponType = "Cannon",
	range = RANGE_MG,
	reloadTime = 6,
	burst = 12,
	burstrate = 0.15,
	weaponVelocity = VELOCITY_MG,
	sprayangle = 200,
	areaofeffect = 32,
	--targetMoveError = 5,
	accuracy = 100,
	
	lineofsight = SMG.areaofeffect,
	
	rgbColor = {0.1, 0.6, 1},
	rgbColor2 = {1, 0.4, 0.4},
	intensity = 0.9,
}