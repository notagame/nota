buildoptions = {}

-- read build options per side from files one by one
local path = "gamedata/configs/sides"
local sideFiles = VFS.DirList(path, "*.lua")
for _, file in pairs(sideFiles) do
	local sideBuildOptions = VFS.Include(file)
	for unitName, buildList in pairs (sideBuildOptions) do
		buildoptions[unitName] = buildList
	end
end

return buildoptions