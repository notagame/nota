local sideData = {
	{
		name = "ARM",
		startUnit = "armbase",
	},
	{
		name = "CORE",
		startUnit = "corbase",
	},
	{
		name = "MERC",
		startUnit = "base_bunker22",
	},
	{
		name = "BUG",
		startUnit = "roost",
	},
}

return sideData