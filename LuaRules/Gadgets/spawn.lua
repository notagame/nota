local moduleInfo = {
	name = "spawn",
	desc = "Spawner of units and projectiles",
	author = "PepeAmpere",
	date = "2019-01-15",
	license = "MIT",
	layer = math.huge, -- higher => lower priority
	enabled = true -- loaded by default?
}

function gadget:GetInfo()
	return moduleInfo
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local HMSF = attach.Module(modules, "hmsf")
local tableExt = attach.Module(modules, "tableExt")
local Vec3 = attach.Module(modules, "vec3")
attach.Module(modules, "spawn")

-- UNSYNCED
if (not gadgetHandler:IsSyncedCode()) then
	return false
end

-- lists
local unitsToSpawn = {}
local unitsToSpawnFiltered = {} -- just helper
local unitsToExpire = {}
local projectilesToSpawn = {}
local projectilesToSpawnFiltered = {} -- just helper
local projectilesToExpire = {}

-- mapping speedups
local weaponDefIDToUnitSpawnDef = {}
local weaponDefIDToProjectileSpawnDef = {}

-- spring API speedups
local SpringCreateUnit = Spring.CreateUnit
local SpringDestroyUnit = Spring.DestroyUnit
local SpringGetGameFrame = Spring.GetGameFrame
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitTeam = Spring.GetUnitTeam
local SpringSetUnitDirection = Spring.SetUnitDirection
local SpringSetUnitNeutral = Spring.SetUnitNeutral
local GAIA_TEAM_ID = Spring.GetGaiaTeamID()

function gadget:Initialize()
	-- ! until its needed we limit the system to just
	-- one registration per weaponDefID for performance reasons
	for objectSpawnName, objectSpawnDef in pairs(spawnObjects) do
		if objectSpawnDef.events ~= nil then
			for eventName, eventData in pairs(objectSpawnDef.events) do
				if (eventName == "explosion") then -- if match explosion event
					local weaponName = eventData.name
					if WeaponDefNames[weaponName] then -- if match weapon name
						local weaponDefID = WeaponDefNames[weaponName].id
						weaponDefIDToUnitSpawnDef[weaponDefID] = objectSpawnDef
						Script.SetWatchWeapon(weaponDefID, true)
						break
					end
				end
			end
		end
	end
	
	-- ! until its needed we limit the system to just
	-- one registration per weaponDefID for performance reasons
	for projectileSpawnName, projectileSpawnDef in pairs(spawnProjectiles) do
		if projectileSpawnDef.events ~= nil then
			for eventName, eventData in pairs(projectileSpawnDef.events) do
				if (eventName == "explosion") then -- if match explosion event
					local weaponName = eventData.name
					if WeaponDefNames[weaponName] then -- if match weapon name
						local weaponDefID = WeaponDefNames[weaponName].id
						weaponDefIDToProjectileSpawnDef[weaponDefID] = projectileSpawnDef
						Script.SetWatchWeapon(weaponDefID, true)
						break
					end
				end
			end
		end
	end
end

function gadget:Explosion(weaponDefID, px, py, pz, creatorUnitID, projectileID)
	
	-- units spawner
	local spawnDef = weaponDefIDToUnitSpawnDef[weaponDefID]
	if spawnDef and creatorUnitID then
		local frame = SpringGetGameFrame()
		local spawnFrame = frame
		
		if (spawnDef.SpawnDefFunction) then
			spawnDef = spawnDef.SpawnDefFunction(
				spawnDef, 
				{
					weaponDefID = weaponDefID, 
					position = Vec3(px, py, pz), 
					creatorUnitID = creatorUnitID, 
					projectileID = projectileID
				}
			)
		end
		if (spawnDef.delay ~= nil) then
			spawnFrame = spawnFrame + spawnDef.delay:ToFrames()
		end
		local expirationFrame = nil
		if (spawnDef.expiration ~= nil) then
			expirationFrame = frame + spawnDef.expiration:ToFrames()
		end
		-- position can be added to spawnDef via SpawnDefFunction
		local spawnPosition = spawnDef.position or Vec3(px, py, pz)
		unitsToSpawn[#unitsToSpawn + 1] = {
			name = spawnDef.name,
			ownerTeamID = SpringGetUnitTeam(creatorUnitID),
			creatorUnitID = creatorUnitID,
			position = spawnPosition,
			direction = spawnDef.direction,
			expirationFrame = expirationFrame,
			spawnFrame = spawnFrame,
			ownerNeutralOverride = spawnDef.ownerNeutralOverride,
			customFunction = spawnDef.SpawnDefFunction,
		}
		
		if (spawnDef.additionalUnits) then
			for i=1, #spawnDef.additionalUnits do
				local additionalDef = spawnDef.additionalUnits[i]
				unitsToSpawn[#unitsToSpawn + 1] = {
					name = additionalDef.name,
					ownerTeamID = additionalDef.ownerTeamID or SpringGetUnitTeam(creatorUnitID),
					creatorUnitID = creatorUnitID,
					position = additionalDef.position or spawnPosition,
					direction = additionalDef.direction,
					expirationFrame = additionalDef.expirationFrame or expirationFrame,
					spawnFrame = additionalDef.spawnFrame or spawnFrame,
					ownerNeutralOverride = additionalDef.ownerNeutralOverride,
					customFunction = additionalDef.SpawnDefFunction,
				}
			end
		end
	end
	
	--[[ no projectile spawners now
	]] -- 
	return false
end

function gadget:GameFrame(frame)
	local spawningCheck = (frame % 4) == 0
	local expirationCheck = (frame % 4) == 2
	if spawningCheck then
	
		-- units spawn from projectiles
		unitsToSpawnFiltered = {}
		for i=1, #unitsToSpawn do
			local spawnDef = unitsToSpawn[i]
			if spawnDef.spawnFrame <= frame then
				local spawnTeamID = spawnDef.ownerTeamID
				if (spawnDef.ownerNeutralOverride) then spawnTeamID = GAIA_TEAM_ID end
				
				local unitID = SpringCreateUnit(
					UnitDefNames[spawnDef.name].id, -- unitDefID
					spawnDef.position.x,
					spawnDef.position.y,
					spawnDef.position.z,
					0, -- 4 directions rotation (we can override below)
					spawnTeamID, -- teamID
					_,_,_,
					spawnDef.creatorUnitID
				)
				
				if (spawnDef.ownerNeutralOverride) then SpringSetUnitNeutral(unitID, true) end
				
				if (spawnDef.direction) then
					local dx,dy,dz = spawnDef.direction:AsSpringVector()
					SpringSetUnitDirection(unitID, dx, dy, dz)
				end
				
				if (spawnDef.expirationFrame) then
					unitsToExpire[unitID] = spawnDef.expirationFrame
				end
			else
				unitsToSpawnFiltered[#unitsToSpawnFiltered + 1] = unitsToSpawn[i]
			end
		end
		
		-- move units back
		unitsToSpawn = {}
		for i=1, #unitsToSpawnFiltered do
			unitsToSpawn[i] = unitsToSpawnFiltered[i]
		end
		
		-- no projectiles spawn from projectiles yet
	end
	

	if expirationCheck then
		
		-- expiration check for units
		for unitID, expirationTime in pairs(unitsToExpire) do
			if expirationTime <= frame then
				SpringDestroyUnit(unitID, true, true)
				unitsToExpire[unitID] = nil
			end
		end
		
		-- no projectiles expiration yet
	end
end
