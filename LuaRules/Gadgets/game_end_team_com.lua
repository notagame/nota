--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--
--  file:    game_end_team_com.lua
--  author:  PepeAmpere
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function gadget:GetInfo()
	return {
		name      = "Team Com End",
		desc      = "Handles team commander end",
		author    = "PepeAmpere",
		date      = "24th June, 2012",
		license   = "BY-NC-SA",
		layer     = 0,
		enabled   = true  --  loaded by default?
	}
end

-- synced only
if (not gadgetHandler:IsSyncedCode()) then
	return false
end

-- MODULES
-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message")

hmsf = attach.Module(modules, "hmsf")
tableExt = attach.Module(modules, "tableExt")
goals = attach.Module(modules, "goals")

local allyTeamList = {}
local teamIDtoAllyID = {} -- teamID => allyID
local teamList = {}
local allianceCommandersCount = {}  -- count of commanders in each ally
local allianceCommandersDestroyed = {}  -- count of commanders in each ally
local allianceMembersIDs = {}
local allianceIndexToAllianceID = {}
local allianceIDToAllianceIndex = {}
local leaderUnits = {
    "armbase",
	"corbase",
	"armcom",
	"armcom2",
	"armbcom",
	"corcom",
	"corcom2",
	"bug1",
	"roost",
	"cormonstaq",
	"cormonstaqeasy",
	"cormonstaqmedium",
	"cormonstaqhard",
	"cormonstaqinsane",
	"herfatboy",
	"repmum",
	"repdrone",
}
local leaderUnitsUnitDefIDs = {}
local destroyAlliance = {}
local stepSizeMax = 10
local spacebugsGame = false

local spDestroyUnit = Spring.DestroyUnit
local spGetTeamInfo = Spring.GetTeamInfo
local spGetTeamUnits = Spring.GetTeamUnits

local GAIA_TEAM_ID = Spring.GetGaiaTeamID()
local _,_,_,_,_,GAIA_ALLY_ID = Spring.GetTeamInfo(GAIA_TEAM_ID)

local function GetAllianceID(teamID)
    local _,_,_,_,_,allyID = Spring.GetTeamInfo(teamID)
	return allyID
end

local function DestroyAllianceByStep(allyID,step)
    local thisAllyMembers = allianceMembersIDs[allianceIDToAllianceIndex[allyID]]
    for i=1,#thisAllyMembers do
		local allTeamUnits = spGetTeamUnits(thisAllyMembers[i])
		
		local newStep = step
		if (#allTeamUnits < newStep) then newStep = #allTeamUnits	end
		
        for j=1,newStep do
			if (allTeamUnits[j] ~= nil) then -- hotfix
				spDestroyUnit(allTeamUnits[j])
			end
        end
		--Spring.Echo(allyID,thisAllyMembers[i])
	end
end

function gadget:GameOver()
	gadgetHandler:RemoveGadget()
end

local function UpdateGoals()
	for allyID, commandersCount in pairs(allianceCommandersCount) do
		if (allyID ~= GAIA_ALLY_ID) then
			local goalNameBase = allyID .. allyID .. "_"
			
			-- first ally-own commanders
			local thisAllyGoals = {
				{
					key = goalNameBase .. "preventDestroyingUnits",
					goalType = "preventDestroyingUnits",
					allianceID = allyID,
					
					unitsText = "command units",
					unitsAll = commandersCount + allianceCommandersDestroyed[allyID],
					unitsDestroyed = allianceCommandersDestroyed[allyID],
				}
			}
		
			for enemyAllyID, enemyCommandersCount in pairs(allianceCommandersCount) do
				-- if not same ally
				-- + avoid making mission goals to fight with gaia
				if (allyID ~= enemyAllyID and enemyAllyID ~= GAIA_ALLY_ID) then
					
					goalNameBase = allyID .. enemyAllyID .. "_"				
					
					-- one enemy ally flags
					thisAllyGoals[#thisAllyGoals + 1] = {
						key = goalNameBase .. "destroyEnemyUnits",
						goalType = "destroyEnemyUnits",
						allianceID = enemyAllyID,
						
						unitsText = "command units",
						unitsAll = enemyCommandersCount + allianceCommandersDestroyed[enemyAllyID],
						unitsDestroyed = allianceCommandersDestroyed[enemyAllyID],
					}
				end
			end
			
			-- save to team info of each team in currently probed ally 
			for teamID, allyID in pairs(teamIDtoAllyID) do
				if allyID == allyID then
					message.SendSyncedInfoTeamPacked(
						"missionGoals",
						message.Encode(thisAllyGoals),
						allyID,
						"allied"
					)
				end
			end
		end
			
	end	
end

function gadget:UnitDestroyed(unitID, unitDefID, unitTeamID)
    local name = UnitDefs[unitDefID].name
    for i=1, #leaderUnits do
        if (name == leaderUnits[i]) then
            local _,_,_,_,_,allyID = Spring.GetTeamInfo(unitTeamID)
			if allianceCommandersDestroyed[allyID] ~= nil then
				allianceCommandersDestroyed[allyID] = allianceCommandersDestroyed[allyID] + 1
			end
        end
	end
end

local function TestCommandUnits()
	allianceCommandersCount = {}
	
	for teamID, allyID in pairs(teamIDtoAllyID) do
		allianceCommandersCount[allyID] = 0
		if (allianceCommandersDestroyed[allyID] == nil) then 
			allianceCommandersDestroyed[allyID] = 0 
		end
	end
	for teamID, allyID in pairs(teamIDtoAllyID) do
		allianceCommandersCount[allyID] = allianceCommandersCount[allyID] + #(Spring.GetTeamUnitsByDefs(teamID, leaderUnitsUnitDefIDs))
	end
	
    for i=1, #allianceMembersIDs do
		local allyID = allianceIndexToAllianceID[i]
		if (allyID ~= GAIA_ALLY_ID) then
			--Spring.Echo(allyID, allianceCommandersCount[allyID])
			if (allianceCommandersCount[allyID] == 0 and (not destroyAlliance[allyID])) then
				destroyAlliance[allyID] = true
			end
			if (destroyAlliance[allyID]) then
				local numberOfKilled = math.random(1,stepSizeMax)
				DestroyAllianceByStep(allyID,numberOfKilled)
			end
		end
	end
end

function gadget:GameFrame(n)
    if ((n % 30) == 14) then
		-- avoid testing first minute if its spacebugs game
		if (spacebugsGame and n < 2700) then
			return
		end
	    TestCommandUnits()
		UpdateGoals()
	end
end

function gadget:Initialize()
	teamList = Spring.GetTeamList()
	
	-- first check if there are spacebugs
	for t=1,#teamList do
		local teamID = teamList[t]
		local someName, _, _, isAITeam, side, allyID = Spring.GetTeamInfo(teamID)
		teamIDtoAllyID[teamID] = allyID
		if (isAITeam) then
			--local _, name, _, shortName, version, options = Spring.GetAIInfo(teamList[t])
			local name = Spring.GetTeamLuaAI(teamID)
			if (name == "Bug: Easy" or name == "Bug: Normal" or name == "Bug: Hard" or name == "Bug: Insane") then
				spacebugsGame = true
			end
		end
	end
	
	-- create unit defs filter
	for i=1, #leaderUnits do
		leaderUnitsUnitDefIDs[#leaderUnitsUnitDefIDs + 1] = UnitDefNames[leaderUnits[i]].id
	end
	
	-- init
	local missionName = Spring.GetModOptions().mission_name  -- name of scenario
	if (missionName == "none") then  --- here have to be this condition, onOption only dont work becouse C bool, second killed in mission
	    Spring.Echo("Team Com End ON")
	    allyTeamList = Spring.GetAllyTeamList()		
		for i=1, #allyTeamList do
			
			local allyID = allyTeamList[i]
		    allianceCommandersCount[i] = 0
			allianceMembersIDs[i] = {}
			allianceIndexToAllianceID[i] = allyID
			allianceIDToAllianceIndex[allyID] = i
			destroyAlliance[allyID] = false
			local counter = 1
			
		    for j=1,#teamList do
				local teamID = teamList[j]
			    local thisTeamAllyID = GetAllianceID(teamID) 
				--Spring.Echo(i,thisTeamAllyID,j)
				if (thisTeamAllyID == allyID) then
				    allianceMembersIDs[i][counter] = teamID
				    --allianceCommandersCount[i] = allianceCommandersCount[i] + 1
					counter = counter + 1	
				end				
			end
			--Spring.Echo(allianceCommandersCount[i])
		end
	else
	    Spring.Echo("Removing Team Com End")
	    gadgetHandler:RemoveGadget()
	end
end
