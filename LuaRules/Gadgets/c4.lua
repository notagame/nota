local moduleInfo = {
	name = "c4",
	desc = "Gadget support for C4 system",
	author = "PepeAmpere",
	date = "2019-01-23",
	license = "MIT",
	layer = math.huge, -- higher => lower priority
	enabled = true -- loaded by default?
}

function gadget:GetInfo()
	return moduleInfo
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local HMSF = attach.Module(modules, "hmsf")
local tableExt = attach.Module(modules, "tableExt")
local Vec3 = attach.Module(modules, "vec3")
local message = attach.Module(modules, "message")
local c4 = attach.Module(modules, "c4")

-- UNSYNCED
if (not gadgetHandler:IsSyncedCode()) then
	local PLAYER_ID = Spring.GetLocalPlayerID()
	local PLAYER_NAME,_,_,TEAM_ID, ALLY_ID = Spring.GetPlayerInfo(PLAYER_ID)
	local function C4FullUpdate(subject, encodedMessage)
		local decodedMsg = message.Decode(encodedMessage)
		local playerID = decodedMsg.playerID
		if
			PLAYER_ID == playerID and
			Script.LuaUI('C4FullUpdate')					
		then
			Script.LuaUI.C4FullUpdate(subject, encodedMessage)
		end	
	end
	local function C4WaypointUpdated(subject, encodedMessage)
		local decodedMsg = message.Decode(encodedMessage)
		local playerID = decodedMsg.waypoint:GetOwnerID()
		local _,_,_,teamID,allyID = Spring.GetPlayerInfo(playerID)
		if
			ALLY_ID == allyID and
			Script.LuaUI('C4WaypointUpdated')					
		then
			Script.LuaUI.C4WaypointUpdated(subject, encodedMessage)
		end	
	end
	local function C4WaypointRemoved(subject, encodedMessage)
		local decodedMsg = message.Decode(encodedMessage)
		local playerID = decodedMsg.waypoint:GetOwnerID()
		local _,_,_,teamID,allyID = Spring.GetPlayerInfo(playerID)
		if
			ALLY_ID == allyID and
			Script.LuaUI('C4WaypointRemoved')					
		then
			Script.LuaUI.C4WaypointRemoved(subject, encodedMessage)
		end	
	end
	local function C4EdgeUpdated(subject, encodedMessage)
		local decodedMsg = message.Decode(encodedMessage)
		local playerID1 = decodedMsg.edge.wp1:GetOwnerID()
		local playerID2 = decodedMsg.edge.wp2:GetOwnerID()
		local _,_,_,_,allyID1 = Spring.GetPlayerInfo(playerID1)
		local _,_,_,_,allyID2 = Spring.GetPlayerInfo(playerID2)
		if
			ALLY_ID == allyID1 and
			ALLY_ID == allyID2 and
			Script.LuaUI('C4EdgeUpdated')					
		then
			Script.LuaUI.C4EdgeUpdated(subject, encodedMessage)
		end	
	end
	
	function gadget:Initialize()
		gadgetHandler:AddSyncAction('C4FullUpdate', C4FullUpdate)
		gadgetHandler:AddSyncAction('C4WaypointUpdated', C4WaypointUpdated)
		gadgetHandler:AddSyncAction('C4WaypointRemoved', C4WaypointRemoved)
		gadgetHandler:AddSyncAction('C4EdgeUpdated', C4EdgeUpdated)
	end
	
	return
end

-- SYNCED
waypoints = {}

-- msg updates
function gadget:RecvLuaMsg(msg, playerID)
	message.Receive(msg, playerID) -- using messageReceiver data structure
end