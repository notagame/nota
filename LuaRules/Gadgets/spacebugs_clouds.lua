local moduleInfo = {
	name = "spacebugs_clouds",
	desc = "Experimental spacebugs mechanics",
	author = "PepeAmpere",
	date = "2019-02-29",
	license = "MIT",
	layer = 0, -- higher => lower priority
	enabled = true -- loaded by default?
}

if (not gadgetHandler:IsSyncedCode()) then
	return
end

local simpleTowers = {}
local advancedTowers = {}

local TOWERS_CONVERSION_STEP = 2 * 60 * 30 -- every 2 mins
local TOWERS_MINIMAL_AGE = 10 * 60 * 30 -- 10 mins
local TOWERS_ADVANCED_MAX = 25
local TOWERS_RANGE_CHECK = 800
local TOWERS_IN_RANGE_MAX = 2
local TOWERS_ACCIDIC_EFFECT_RANGE = TOWERS_RANGE_CHECK - 200
local TOWERS_ACCIDIC_DAMAGE = 2
local BASIC_EFFECT = 1
local MAX_EFFECT = 20
local SIMPLE_TOWER = "chickend"
local ADVANCED_TOWER = "bugtower"

local SpringAddUnitDamage = Spring.AddUnitDamage
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitTeam = Spring.GetUnitTeam
local SpringGetUnitRulesParam = Spring.GetUnitRulesParam
local SpringGetUnitsInCylinder = Spring.GetUnitsInCylinder
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SpringSetUnitHealth = Spring.SetUnitHealth

local unitsGoodEfect = {
	bug1 = true,
	bug2 = true,
	bug3 = true,
	bug5 = true,
	corgamma = true,
	corgamma2 = true,
	cormonsta = true,
	cormonstab = true,
	cormonstaq = true,
	cormonstaqeasy = true,
	cormonstaqmedium = true,
	cormonstaqhard = true,
	cormonstaqinsane = true,
	wormy = true,
	roost = true,
	chickend = true,
	bugtower = true,
}

local function ConvertTower(unitID, conversionFrame)
	local x,y,z = SpringGetUnitPosition(unitID)
	local teamID = SpringGetUnitTeam(unitID)
	
	local listOfUnits = SpringGetUnitsInRectangle(
		x - TOWERS_RANGE_CHECK,
		z - TOWERS_RANGE_CHECK,
		x + TOWERS_RANGE_CHECK,
		z + TOWERS_RANGE_CHECK	
	) -- ignoring teams, conversion is based on biom exploitation ;)
	local advancedTowersCount = 0
	for i=1, #listOfUnits do
		local unitDefID = SpringGetUnitDefID(listOfUnits[i])
		local unitDef = UnitDefs[unitDefID]
		if unitDef.name == ADVANCED_TOWER then
			advancedTowersCount = advancedTowersCount + 1
		end
	end
	
	if advancedTowersCount <= TOWERS_IN_RANGE_MAX then
		Spring.DestroyUnit(unitID)
		local newUnitID = Spring.CreateUnit(
			ADVANCED_TOWER,
			x,y,z,
			(math.random(4) - 1),
			teamID
		)
		
		advancedTowers[newUnitID] = BASIC_EFFECT
	end
end

local function AccidEffectOnArea(middleX, middleZ, value)
	local listOfUnits = SpringGetUnitsInCylinder(
		middleX, middleZ, 
		TOWERS_ACCIDIC_EFFECT_RANGE
	)
	for i=1, #listOfUnits do
		local unitID = listOfUnits[i]
		local unitDefID = SpringGetUnitDefID(listOfUnits[i])
		local unitDef = UnitDefs[unitDefID]
		if unitsGoodEfect[unitDef.name] then
			local health, maxHealth = SpringGetUnitHealth(unitID)
			local onfire = SpringGetUnitRulesParam(unitID, "on_fire")
			if 
				health < maxHealth and
				(
					onfire == nil or
					onfire == 0
				)
			then
				local newHealth = math.min(health + math.max(math.floor(maxHealth / 100), TOWERS_ACCIDIC_DAMAGE*5), maxHealth)
				SpringSetUnitHealth(unitID, newHealth)
			end
		else
			SpringAddUnitDamage(unitID, TOWERS_ACCIDIC_DAMAGE * value)
		end
	end
end

function gadget:UnitCreated(unitID, unitDefID, unitTeam)
	local unitDefID = SpringGetUnitDefID(unitID)
	local unitDef = UnitDefs[unitDefID]
	if unitDef.name == ADVANCED_TOWER then
		advancedTowers[unitID] = BASIC_EFFECT
	end
end

function gadget:GameFrame(frame)
	if frame % TOWERS_CONVERSION_STEP == 0 then
		local advancedTowersCount = 0
		for k,v in pairs(advancedTowers) do
			advancedTowersCount = advancedTowersCount + 1
		end
	
		local allUnits = Spring.GetAllUnits()
		for i=1, #allUnits do
			local unitID = allUnits[i]
			if 
				simpleTowers[unitID] == nil and
				advancedTowers[unitD] == nil
			then
				local unitDefID = SpringGetUnitDefID(unitID)
				local unitDef = UnitDefs[unitDefID]
				if unitDef.name == SIMPLE_TOWER then
					simpleTowers[unitID] = frame
				end
			elseif 
				simpleTowers[unitID] ~= nil and
				frame - simpleTowers[unitID] > TOWERS_MINIMAL_AGE and
				advancedTowersCount < TOWERS_ADVANCED_MAX and
				(math.random() < 0.2)
			then
				ConvertTower(unitID, frame)
			end
		end
	end	
	
	if frame % 30 == 0 then
		for unitID, value in pairs(advancedTowers) do
			local x,y,z = SpringGetUnitPosition(unitID)
			AccidEffectOnArea(x, z, value)
		end
	end
	
	if frame % 120 == 0 then
		for unitID, value in pairs(advancedTowers) do
			advancedTowers[unitID] = math.max(1, value - 1)
		end
	end
end

function gadget:UnitDestroyed(unitID, unitDefID, unitTeam, attackerID, attackerDefID, attackerTeam)
	local unitDef = UnitDefs[unitDefID]
	if unitDef.name == SIMPLE_TOWER then
		simpleTowers[unitID] = nil
	end
	if unitDef.name == ADVANCED_TOWER then
		advancedTowers[unitID] = nil
	end
end

function gadget:UnitPreDamaged(unitID, unitDefID, unitTeam, damage, paralyzer, weaponDefID, projectileID, attackerID, attackerDefID, attackerTeam)
	local unitDef = UnitDefs[unitDefID]
	if 
		unitDef.name == ADVANCED_TOWER and
		advancedTowers[unitID] ~= nil
	then
		advancedTowers[unitID] = math.min(MAX_EFFECT, advancedTowers[unitID] + 1)
	end
end