local teleportDefs = {
	armpod = {
		range = 1400,
		height = 0,
		speed = 100, 
		reload = 45, -- optional
		energyCost = 1000
	}
}

local queens = {
	"cormonstaq",
	"cormonstaqeasy",
	"cormonstaqmedium",
	"cormonstaqhard",
	"cormonstaqinsane",
}
for i=1, #queens do
	teleportDefs[queens[i]] = {
		range = 1400,
		height = 0,
		speed = 100, 
		reload = 16, -- optional
		energyCost = 0,
	}
end

return teleportDefs