-- config file storing the relative layer level for selected widgets
-- motivation is to handle mainly the mouse clicks in a controlled manner

-- higher value => lower priority
local layersPerWidgetName = {
	["Nota UI Minimap"] = 900,
	["C4 GUI"] = 1003,
	["TacticalSelection"] = 1010,
	["Select army only"] = 1020,
}

return layersPerWidgetName