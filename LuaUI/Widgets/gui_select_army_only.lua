local layers = VFS.Include("LuaUI/configs/UIlayers.lua")
local WIDGET_NAME = "Select army only"
if (layers[WIDGET_NAME] == nil) then Spring.Echo("WARNING: layer definition for [" .. WIDGET_NAME .. "] is missing.") end

function widget:GetInfo()
	return {
		name = WIDGET_NAME,
		desc = "If area select is used and if at least 1 non-builder non-building unit in a selection, all builders deselected",
		author = "PepeAmpere",
		date = "2nd July, 2013",
		license = "MIT",
		layer = layers[WIDGET_NAME],
		enabled = true -- loaded by default?
	}
end

local SpringGetSpectatingState = Spring.GetSpectatingState
local spectator = SpringGetSpectatingState()
if spectator then
	return
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local message = attach.Module(modules, "message")
local Vec3 = attach.Module(modules, "vec3")
local HMSF = attach.Module(modules, "hmsf")

local bases = {
	armbase = true,
	arm2veh = true,
	arm2air = true,
	arm2def = true,
	arm2kbot = true,
	armlvl2 = true,
	armnanotc = true,
	corbase = true,
	cor2air = true,
	cor2def = true,
	cor2kbot = true,
	cor2veh = true,
	corlvl2 = true,
	corntow = true,
}
local builders = {
	armfark = true,
	armcv = true,
	armca = true,
	armch = true,
	armacsub = true,
	armck = true,
	cornecro = true,
	corcv = true,
	corca = true,
	corch = true,
	coracsub = true,
	corack = true,
}
local commanders = {
	armbcom = true,
	armcom = true,
	armcom2 = true,
	corcom = true,
	corcom2 = true,
}
local crawlingMines = {
	armvader = true,
	corroach = true,
}
local special = {
	cortraq = true,
	corsabo = true,
}
local nonArmyUnit = {}
local gameStarted = false
local godMode = false

local unitDefIDToSize = {}

local SpringGetActiveCommand = Spring.GetActiveCommand
local SpringGetCameraPosition = Spring.GetCameraPosition
local SpringGetGameFrame = Spring.GetGameFrame
local SpringGetMouseState = Spring.GetMouseState
local SpringGetPlayerInfo = Spring.GetPlayerInfo
local SpringGetPlayerList = Spring.GetPlayerList
local SpringGetSelectedUnits = Spring.GetSelectedUnits
local SpringGetSelectedUnitsSorted = Spring.GetSelectedUnitsSorted
local SpringGetTeamColor = Spring.GetTeamColor
local SpringGetUnitBasePosition = Spring.GetUnitBasePosition
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetVisibleUnits = Spring.GetVisibleUnits
local SpringIsGodModeEnabled = Spring.IsGodModeEnabled
local SpringIsSphereInView = Spring.IsSphereInView
local SpringSelectUnitArray = Spring.SelectUnitArray
local SpringTraceScreenRay = Spring.TraceScreenRay

local glBeginEnd = gl.BeginEnd
local glBillboard = gl.Billboard
local glColor = gl.Color
local glDepthTest = gl.DepthTest
local glDrawGroundCircle = gl.DrawGroundCircle
local glLineWidth = gl.LineWidth
local glLineStipple = gl.LineStipple
local glPopMatrix = gl.PopMatrix
local glPushMatrix = gl.PushMatrix
local glText = gl.Text
local glTranslate = gl.Translate
local glVertex = gl.Vertex

local PLAYER_ID = Spring.GetLocalPlayerID()
local PLAYER_NAME,_,_,PLAYER_TEAM_ID, PLAYER_ALLY_ID = SpringGetPlayerInfo(PLAYER_ID)
local PLAYER_TEAM_ID_CHECKED = PLAYER_TEAM_ID
local SELECTION_DISTANCE_MAX = 400
local SELECTION_DISTANCE_PER_SIZE = 7
local DOUBLE_CLICK_TOLERANCE_DISTANCE = 10
local DOUBLE_CLICK_TOLERANCE_TIME_BETWEEN_CLICKS = HMSF(0,0,0,10) -- frames
local DOUBLE_CLICK_TOLERANCE_TIME_BETWEEN_CLICK_AND_RELEASE = HMSF(0,0,0,2) -- frames

local KEY_SHIFT = 304
local KEY_CONTROL = 306
local KEY_ALT = 308
local KEY_DELETE = 127
local KEY_SPACE = 32

local SELECTION_UPDATE_SUBJECT = "c4_alliedSelection"

function widget:Initialize()
	for unitDefID, unitDef in pairs (UnitDefs) do
		local unitName = unitDef.name
		if 
			unitDef.isBuilding or
			unitDef.canFly or
			builders[unitName] or
			bases[unitName] or
			commanders[unitName] or
			crawlingMines[unitName] or
			special[unitName]
		then
			nonArmyUnit[unitDefID] = true
		end
		
		-- cache the sizes of units
		unitDefIDToSize[unitDefID] = math.max(unitDef.xsize, unitDef.zsize)
	end
end

local SELUI = {
	pressPosition = nil,
	pressTime = nil,
	previousPressPosition = nil,
	previousPressTime = nil,
	shiftPressed = false,
}

function widget:KeyPress(key, mods, isRepeat)
	if not isRepeat then
		if (key == KEY_SHIFT) then
			SELUI.shiftPressed = true
		end
		if (key == KEY_SPACE) then
			SELUI.spacePressed = true
		end
		if (key == KEY_CONTROL) then
			SELUI.ctrlPressed = true
		end
	end
end

function widget:KeyRelease(key)
	if (key == KEY_SHIFT) then
		SELUI.shiftPressed = false
	end
	if (key == KEY_SPACE) then
		SELUI.spacePressed = false
	end
	if (key == KEY_CONTROL) then
		SELUI.ctrlPressed = false
	end
end

function widget:MousePress(mouseX, mouseY, button)
	-- update constants in case of developer switching over teams contol
	PLAYER_NAME,_,_,PLAYER_TEAM_ID, PLAYER_ALLY_ID = SpringGetPlayerInfo(PLAYER_ID)
	
	-- logic itself
	if 
		gameStarted and
		not godMode
	then
		SELUI.pressTime = HMSF(0, 0, 0, SpringGetGameFrame())
		
		local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
		local descOutside, mapCoordsOutside = SpringTraceScreenRay(mouseX, mouseY, true, false, true)
		-- Spring.Echo(mapCoordsOutside[4], mapCoordsOutside[5], mapCoordsOutside[6])
		local selectX, selectY, selectZ
		if mapCoords ~= nil then -- is it on the map?
			selectX, selectY, selectZ = mapCoords[1], mapCoords[2], mapCoords[3]
		else
			selectX, selectY, selectZ = mapCoordsOutside[4], mapCoordsOutside[5], mapCoordsOutside[6]
		end		
		-- local selectedUnits = SpringGetSelectedUnits()
		local index, cmdID, cmdType, cmdName = SpringGetActiveCommand() 
		if (
			-- + not command giving!
			cmdID == nil and
			button == 1 -- LMB
		) then	
			SELUI.pressPosition = Vec3(selectX, selectY, selectZ)
			if 
				SELUI.previousPressTime ~= nil and
				SELUI.pressTime - SELUI.previousPressTime > DOUBLE_CLICK_TOLERANCE_TIME_BETWEEN_CLICKS 
			then
				SELUI.previousPressPosition = nil
				SELUI.previousPressTime = nil
			end
			return true
		else
			SELUI.pressPosition = nil	
			SELUI.previousPressPosition = nil
			SELUI.previousPressTime = nil
		end
	end
end

local function UpdateSelection(listOfUnits, append)
	SpringSelectUnitArray(listOfUnits, append)
	local actualSelection = SpringGetSelectedUnits()
	local selectionMap = {}
	for i=1, #actualSelection do
		local unitID = actualSelection[i]
		selectionMap[unitID] = unitDefIDToSize[SpringGetUnitDefID(unitID)] -- save size
		-- Spring.Echo(unitID, selectionMap[unitID])
	end
	message.SendUI(
		{
			subject = SELECTION_UPDATE_SUBJECT,
			selectionMap = selectionMap,
		},
		"allies"
	)
	-- message.SendUI( 
		-- {
			-- subject = SELECTION_UPDATE_SUBJECT,
			-- selectionMap = selectionMap,
		-- },
		-- "specs"
	-- )
	-- playersUnitsSelection[PLAYER_ID] = selectionMap
end

function widget:MouseRelease()
	local releaseTime = HMSF(0, 0, 0, SpringGetGameFrame())
	
	local unitsInArea = {}
	local armyList = {}
	local nonArmyList = {}
	
	local mouseX, mouseY, lmbPressed, middlePressed, rmbPressed, offscreen = SpringGetMouseState() 
	local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
	local descOutside, mapCoordsOutside = SpringTraceScreenRay(mouseX, mouseY, true, false, true)
	local releaseX, releaseY, releaseZ
	if mapCoords ~= nil then -- is it on the map?
		releaseX, releaseY, releaseZ = mapCoords[1], mapCoords[2], mapCoords[3]
	else
		releaseX, releaseY, releaseZ = mapCoordsOutside[4], mapCoordsOutside[5], mapCoordsOutside[6]
	end
	local pressPosition = SELUI.pressPosition
	local pressX, pressY, pressZ = pressPosition:GetCoordinates()
	local selectedUnits = SpringGetSelectedUnits()
	if 
		(
			SELUI.previousPressPosition == nil and
			SELUI.pressPosition:Distance2D(Vec3(releaseX, releaseY, releaseZ)) < DOUBLE_CLICK_TOLERANCE_DISTANCE
		)
		or
		(
			selectedUnits[1] ~= nil and
			SELUI.previousPressPosition ~= nil and
			SELUI.previousPressPosition:Distance2D(SELUI.pressPosition) < DOUBLE_CLICK_TOLERANCE_DISTANCE
		)
	then		
		local camX, camY, camZ = SpringGetCameraPosition()
		local selectionDistanceMax = SELECTION_DISTANCE_MAX * (camY/1000)
		local selectionDistancePerSize = SELECTION_DISTANCE_PER_SIZE * (camY/1000)
		
		unitsInArea = SpringGetUnitsInRectangle(
			math.min(pressX, releaseX)-selectionDistanceMax,
			math.min(pressZ, releaseZ)-selectionDistanceMax,
			math.max(pressX, releaseX)+selectionDistanceMax,
			math.max(pressZ, releaseZ)+selectionDistanceMax,
			PLAYER_TEAM_ID_CHECKED
		)
		local minDistance = math.huge
		local minDistanceUnitID = nil
		local minDistanceUnitDefID = nil
		if (unitsInArea[1] ~= nil) then				
			for i=1, #unitsInArea do
				local unitID = unitsInArea[i]
				local unitDefID = SpringGetUnitDefID(unitID)
				local x,y,z = SpringGetUnitPosition(unitID)
				local unitPosition = Vec3(x,y,z)
				local distance2D = unitPosition:DistanceToLine(pressPosition, Vec3(camX, camY, camZ))
				local transporterID = SpringGetUnitTransporter(unitID)
				if 
					distance2D < minDistance and
					distance2D < unitDefIDToSize[unitDefID] * selectionDistancePerSize and
					transporterID == nil
				then
					minDistanceUnitID = unitID
					minDistanceUnitDefID = unitDefID
					minDistance = distance2D
				end
			end
		end
		
		--Spring.Echo(minDistanceUnitID)
		if 
			minDistanceUnitID ~= nil and
			minDistanceUnitDefID ~= nil
		then
			if 
				SELUI.previousPressTime ~= nil and
				(releaseTime - SELUI.pressTime) <= DOUBLE_CLICK_TOLERANCE_TIME_BETWEEN_CLICK_AND_RELEASE
			then					
				unitsInArea = {}
				local visibleUnits = SpringGetVisibleUnits(PLAYER_TEAM_ID_CHECKED)
				for i=1, #visibleUnits do
					local unitID = visibleUnits[i]
					local unitDefID = SpringGetUnitDefID(unitID)
					if unitDefID == minDistanceUnitDefID then
						unitsInArea[#unitsInArea + 1] = unitID
					end
				end
				
				-- select as via doubleclick
				UpdateSelection(unitsInArea, SELUI.shiftPressed)
				SELUI.previousPressPosition = nil
				SELUI.previousPressTime = nil
			else
				-- select as via singleclick
				UpdateSelection({minDistanceUnitID}, SELUI.shiftPressed)
				SELUI.previousPressPosition = SELUI.pressPosition
				SELUI.previousPressTime = SELUI.pressTime
			end			
			
			-- selection completed
			SELUI.pressPosition = nil
			SELUI.pressTime = nil
		else
			-- deselected
			UpdateSelection({}, SELUI.shiftPressed)
			SELUI.previousPressPosition = nil
			SELUI.previousPressTime = nil
			SELUI.pressPosition = nil
			SELUI.pressTime = nil
		end
	else			
		unitsInArea = SpringGetUnitsInRectangle(
			math.min(pressX, releaseX),
			math.min(pressZ, releaseZ),
			math.max(pressX, releaseX),
			math.max(pressZ, releaseZ),
			PLAYER_TEAM_ID_CHECKED
		)
		
		local isArmy = false
		for i=1, #unitsInArea do
			local unitID = unitsInArea[i]
			local unitDefID = SpringGetUnitDefID(unitID)
			if UnitDefs[unitDefID] ~= nil then
				if nonArmyUnit[unitDefID] == nil then
					armyList[#armyList + 1] = unitID
					isArmy = true
				else
					nonArmyList[#nonArmyList + 1] = unitID				
				end
			end
		end
		
		if 
			isArmy
		then -- select filtered list
			UpdateSelection(armyList, SELUI.shiftPressed)
		else -- select all
			UpdateSelection(unitsInArea, SELUI.shiftPressed)
		end
		
		SELUI.previousPressPosition = nil
		SELUI.previousPressTime = nil
		SELUI.pressPosition = nil
		SELUI.pressTime = nil
	end
	SELUI.pressPosition = nil
	SELUI.pressTime = nil
end

function widget:GameFrame(frame)
	godMode = SpringIsGodModeEnabled()
	if godMode then
		PLAYER_TEAM_ID_CHECKED = nil
	else
		PLAYER_TEAM_ID_CHECKED = PLAYER_TEAM_ID
	end
	
	if not gameStarted then -- fallback if widget reloaded during the game
		gameStarted = true
	end
end

function widget:GameStart()
	gameStarted = true
end

-- drawing part
function widget:DrawWorld()
	if SELUI.pressPosition ~= nil then
		local mouseX, mouseY, lmbPressed, middlePressed, rmbPressed, offscreen = SpringGetMouseState() 
		local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
		local descOutside, mapCoordsOutside = SpringTraceScreenRay(mouseX, mouseY, true, false, true)
		local releaseX, releaseY, releaseZ
		if mapCoords ~= nil then -- is it on the map?
			releaseX, releaseY, releaseZ = mapCoords[1], mapCoords[2], mapCoords[3]
		else
			releaseX, releaseY, releaseZ = mapCoordsOutside[4], mapCoordsOutside[5], mapCoordsOutside[6]
		end
		local pressX, pressY, pressZ = SELUI.pressPosition:GetCoordinates()
		local height = pressY
		glLineWidth(2)
		glColor(0.4, 0.8, 0.4, 0.7)
		glBeginEnd(GL.LINES,
			function()
				glVertex(pressX, height, pressZ)
				glVertex(releaseX, height, pressZ)
			end
		)
		glBeginEnd(GL.LINES,
			function()
				glVertex(pressX, height, pressZ)
				glVertex(pressX, height, releaseZ)
			end
		)
		glBeginEnd(GL.LINES,
			function()
				glVertex(pressX, height, releaseZ)
				glVertex(releaseX, height, releaseZ)
			end
		)
		glBeginEnd(GL.LINES,
			function()
				glVertex(releaseX, height, pressZ)
				glVertex(releaseX, height, releaseZ)
			end
		)
	end
	-- local actualSelection = SpringGetSelectedUnits()
	-- local selectionMap = {}
	-- for i=1, #actualSelection do
		-- local unitID = actualSelection[i]
		-- selectionMap[unitID] = unitDefIDToSize[SpringGetUnitDefID(unitID)] -- save size
		-- local x, y, z = SpringGetUnitBasePosition(unitID)
		-- glDrawGroundCircle(x, y, z, unitDefIDToSize[SpringGetUnitDefID(unitID)]*10, SELECTION_CIRCLE_SEGMENTS)	
	-- end	
end
--[[
function widget:DrawWorldPreUnit()
	glColor(0.0, 1.0, 0.0, 1.0)
	glLineWidth(SELECTION_CIRCLE_WIDTH)
	
	for playerID, selectionMap in pairs(playersUnitsSelection) do
		local teamID = playerIDToTeamID[playerID]
		local teamColor = teamColors[teamID]
				
		for unitID, size in pairs(selectionMap) do
			local x, y, z = SpringGetUnitBasePosition(unitID)
			local inView = false
			if z ~= nil then
				inView = SpringIsSphereInView(x, y, z, size)
			end
			if inView then
				glColor(teamColor[1], teamColor[2], teamColor[3], SELECTION_CIRCLE_ALPHA)
				glDrawGroundCircle(x, y, z, size*7, SELECTION_CIRCLE_SEGMENTS)
				glColor(teamColor[1], teamColor[2], teamColor[3], SELECTION_CIRCLE_ALPHA-0.1)
				glDrawGroundCircle(x, y, z, size*6, SELECTION_CIRCLE_SEGMENTS)
			end
		end
	end
	
	-- local myColor = teamColors[PLAYER_TEAM_ID]
	-- glColor(myColor[1], myColor[2], myColor[3], SELECTION_CIRCLE_ALPHA)  
	
	-- local selectionMap = playersUnitsSelection[PLAYER_ID]
	-- local selectionMap = {}
	-- for unitID, size in pairs(selectionMap) do
		-- local x, y, z = SpringGetUnitBasePosition(unitID)
		-- glDrawGroundCircle(x, y, z, size*20, SELECTION_CIRCLE_SEGMENTS)
	-- end
		
 	glColor(1, 1, 1, 1)
	glLineWidth(1)
end
]]--