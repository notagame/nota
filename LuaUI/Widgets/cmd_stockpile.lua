local moduleInfo = {
	name = "Autostockpiler",
	desc = "Widget trigger stockpiling weapons",
	author = "PepeAmpere",
	date = "2020-03-01",
	license = "MIT",
	layer = 10000, -- higher => lower priority
	enabled = true -- loaded by default?
}

function widget:GetInfo()
	return moduleInfo
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local message = attach.Module(modules, "message")
VFS.Include('LuaRules/Configs/commandsIDs.lua')

local CMD_STOCKPILE = CMD.STOCKPILE
local PLAYER_ID = Spring.GetLocalPlayerID()
local PLAYER_NAME,_,_,TEAM_ID = Spring.GetPlayerInfo(PLAYER_ID)

autofill = {
	[CMD_PLANTBOMB] = true,
	[CMD_PLANTMINEFIELD] = true
}

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function AddToStockpile(unitID, params)
	SpringGiveOrderToUnit(
		unitID,
		CMD_STOCKPILE,
		{1},
		params
	)
end

function widget:UnitCreated(unitID, unitDefID, unitTeam, builderID)
	local unitDef = UnitDefs[unitDefID]
	if
		unitDef and
		unitTeam == TEAM_ID and
		unitDef.canStockpile
	then
		AddToStockpile(unitID, {"ctrl", "shift"})
	end
end

local function HeroesAbilityActivation(subject, encodedMessage)
	local decodedMsg = message.Decode(encodedMessage)
	local cmdID = decodedMsg.cmdID
	local unitID = decodedMsg.unitID
	if autofill[cmdID] then
		AddToStockpile(unitID, {})
	end
end

function widget:Initialize()
	-- handlers for events from gadget
	widgetHandler:RegisterGlobal('HeroesAbilityActivation', HeroesAbilityActivation)
end
