local layers = VFS.Include("LuaUI/configs/UIlayers.lua")
local WIDGET_NAME = "C4 GUI"
if (layers[WIDGET_NAME] == nil) then Spring.Echo("WARNING: layer definition for [" .. WIDGET_NAME .. "] is missing.") end

local moduleInfo = {
	name = WIDGET_NAME,
	desc = "Widget support for C4 system",
	author = "PepeAmpere",
	date = "2019-01-23",
	license = "MIT",
	layer = layers[WIDGET_NAME], -- higher => lower priority
	enabled = true -- loaded by default?
}

function widget:GetInfo()
	return moduleInfo
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local HMSF = attach.Module(modules, "hmsf")
local tableExt = attach.Module(modules, "tableExt")
local Vec3 = attach.Module(modules, "vec3")
local POI = attach.Module(modules, "poi")
local message = attach.Module(modules, "message")
local c4 = attach.Module(modules, "c4")

-- UI variables
local WPUI = {
	selected = nil,
	selectedHold = false,
	shiftPressed = false,
	creationPosition = nil,
	locked = true,
	lockAlpha = 0.2,
	debug = false,
}
waypoints = {}
-- temp
unitsRelativePositions = {}
unitsFinalPositions = {}
unitsLastPositions = {}
unitIDToOrderID = {}
unitIDCanReclaim = {}
orderIDToState = {}
ordersToReset = {}
unitIDToNotMovingTime = {}
unitIDDistanceToFinalPosition = {}

-- speedups
local SpringGetGameSeconds = Spring.GetGameSeconds
local SpringGetMouseState = Spring.GetMouseState
local SpringGetPlayerInfo = Spring.GetPlayerInfo
local SpringGetUnitCommands = Spring.GetUnitCommands
local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringTraceScreenRay = Spring.TraceScreenRay
local SpringValidUnitID = Spring.ValidUnitID

local FindWaypoint = c4.FindWaypoint

local glBeginEnd = gl.BeginEnd
local glBillboard = gl.Billboard
local glColor = gl.Color
local glDrawGroundCircle = gl.DrawGroundCircle
local glLineWidth = gl.LineWidth
local glLineStipple = gl.LineStipple
local glPopMatrix = gl.PopMatrix
local glPushMatrix = gl.PushMatrix
local glText = gl.Text
local glTranslate = gl.Translate
local glVertex = gl.Vertex

-- ARGB?
local fontBlack = "\255\001\001\001" 
local fontBlue = "\255\001\001\255"
local fontWhite = "\255\255\255\255"
local fontDebugGrey = "\255\220\220\220"
local fontColorReset = "\b " -- !!! space at the end important

local PLAYER_ID = Spring.GetLocalPlayerID()
local PLAYER_NAME,_,_,TEAM_ID = Spring.GetPlayerInfo(PLAYER_ID)
local LOCAL_LAYER_ID = 1 -- we have just one layer now

local ALPHA_LOCKED = 0.2
local ALPHA_UNLOCKED = 1.0
local WAYPOINT_SIZE = 32
local WAYPOINT_SELECTION_DISTANCE = math.floor(WAYPOINT_SIZE * 1.5)
local WAYPOINT_OWNER_DISTANCE = WAYPOINT_SELECTION_DISTANCE
local WAYPOINT_DEBUG_LINE = 10
local WAYPOINT_ARROW_LENGTH = WAYPOINT_SIZE * 2
local CIRCLE_SEGMENTS = 16
local WAYPOINT_COLORS_SELECTED = 1000
local WAYPOINT_COLORS_SELECTED_AND_HOLD = 1001
local WAYPOINT_COLORS_NEW = 1002
local WAYPOINT_COLORS_MINE = 1010
local WAYPOINT_COLORS_NOT_MINE = 1011
local WAYPOINTS_COLORS = { -- nodeDefID => color
	[1] = function() glColor(0.8, 0.8, 0.8, WPUI.lockAlpha) end,
	[2] = function() glColor(1.0, 0.4, 0.4, WPUI.lockAlpha) end,
	[3] = function() glColor(0.8, 0.8, 0.8, WPUI.lockAlpha) end,
	[WAYPOINT_COLORS_SELECTED] = function() glColor(0.9, 0.9, 0.9, WPUI.lockAlpha) end,
	[WAYPOINT_COLORS_SELECTED_AND_HOLD] = function() glColor(1.0, 1.0, 1.0, WPUI.lockAlpha) end,
	[WAYPOINT_COLORS_MINE] = function() glColor(0.6, 1.0, 0.6, WPUI.lockAlpha) end,
	[WAYPOINT_COLORS_NOT_MINE] = function() glColor(1.0, 0.6, 0.6, WPUI.lockAlpha) end,
	[WAYPOINT_COLORS_NEW] = function() glColor(0.5, 0.8, 0.5, WPUI.lockAlpha) end,
}
local WAYPOINT_DEFS = {
	[1] = {
		defID = 1, 
		name = "move",
		drawFunction = function(wp, wpx, wpy, wpz)
			if WPUI.selected == wp then
				if WPUI.selectedHold then
					WAYPOINTS_COLORS[WAYPOINT_COLORS_SELECTED_AND_HOLD]()
				else
					WAYPOINTS_COLORS[WAYPOINT_COLORS_SELECTED]()
				end
			else
				WAYPOINTS_COLORS[wp.defID]()
			end
			glDrawGroundCircle(wpx, wpy, wpz, WAYPOINT_SIZE, CIRCLE_SEGMENTS)
		end,
	},
	[2] = {
		defID = 2, 
		name = "target",
		drawFunction = function(wp, wpx, wpy, wpz)
			WAYPOINTS_COLORS[wp.defID]()
			glDrawGroundCircle(wpx, wpy, wpz, WAYPOINT_SIZE, CIRCLE_SEGMENTS)
		end,
	},
	[3] = {
		defID = 3, 
		name = "area",
		drawFunction = function(wp, wpx, wpy, wpz)
			if WPUI.selected == wp then
				if WPUI.selectedHold then
					WAYPOINTS_COLORS[WAYPOINT_COLORS_SELECTED_AND_HOLD]()
				else
					WAYPOINTS_COLORS[WAYPOINT_COLORS_SELECTED]()
				end
			else
				WAYPOINTS_COLORS[wp.defID]()
			end
			glDrawGroundCircle(wpx, wpy, wpz, 6*WAYPOINT_SIZE, CIRCLE_SEGMENTS)
		end,
	},
}
local EDGES_COLORS_DEFAULT = 0
local EDGES_COLORS_NEW = WAYPOINT_COLORS_NEW
local EDGES_COLORS = { -- edgeDefID => color
	[EDGES_COLORS_DEFAULT] = function() glColor(0.7, 0.7, 0.7, WPUI.lockAlpha) end,
	[1] = function() glColor(0.7, 0.7, 0.7, WPUI.lockAlpha) end,
	[2] = function() glColor(0.5, 0.0, 0.5, WPUI.lockAlpha) end,
	[EDGES_COLORS_NEW] = function() glColor(0.5, 0.8, 0.5, WPUI.lockAlpha) end,
}
local fontOwner = gl.LoadFont(
	"FreeSansBold.otf"
)
local fontDebug = gl.LoadFont(
	"FreeSansBold.otf",
	WAYPOINT_DEBUG_LINE,
	15,
	15
)
local KEY_SHIFT = 304
local KEY_CONTROL = 306
local KEY_ALT = 308
local KEY_DELETE = 127
local KEY_SPACE = 32
local KEY_Q = 113

local WAYPOINT_REACHED_RADIUS = 100
local MOVE_COMPLETE_THRESHOLD_SMALL = 2
local MOVE_COMPLETE_THRESHOLD_HIGH = 20
local RECLAIM_DISTANCE_MIN = 250
local AI_FRAME_MIN = 15
local aiFrameLength = AI_FRAME_MIN

local function RequestAllWaypoints()
	message.SendRules({
			subject = "RequestAllWaypoints"
		}
	)
end
local function RequestAddWaypoint(waypoint)
	message.SendRules({
			subject = "RequestAddWaypoint",
			waypoint = waypoint,
		}
	)
end
local function RequestRemoveWaypoint(waypoint)
	message.SendRules({
			subject = "RequestRemoveWaypoint",
			waypoint = waypoint,
		}
	)
end
local function RequestUpdateWaypoint(waypoint)
	message.SendRules({
			subject = "RequestUpdateWaypoint",
			waypoint = waypoint,
		}
	)
end
local function RequestAddEdge(wp1, wp2, edgeData)
	message.SendRules({
			subject = "RequestAddEdge",
			edge = {
				wp1 = wp1,
				wp2 = wp2,
				edgeData = edgeData,
			},
		}
	)
end
local function RequestRemoveEdge(wp1, wp2)
	message.SendRules({
			subject = "RequestRemoveEdge",
			edge = {
				wp1 = wp1,
				wp2 = wp2,
			},
		}
	)
end
local function UpdateGroup(units)
	message.SendUI({
		subject = "C4UpdateGroup",
		playerID = PLAYER_ID,
		units = units,
	})
end

local function FullUpdate(subject, encodedMessage)
	local decodedMsg = message.Decode(encodedMessage)
	local newWaypoints = decodedMsg.waypoints
	waypoints = {}
	for ID, wp in pairs(newWaypoints) do
		waypoints[ID] = wp
	end
end

local function EdgeUpdated(subject, encodedMessage)
	local decodedMsg = message.Decode(encodedMessage)
	local edge = decodedMsg.edge
	local wp1 = edge.wp1
	local wp2 = edge.wp2
	waypoints[wp1:GetID()] = wp1
	waypoints[wp2:GetID()] = wp2
end

local function WaypointUpdated(subject, encodedMessage)
	local decodedMsg = message.Decode(encodedMessage)
	local wp = decodedMsg.waypoint
	local ID = wp:GetID()
	for orderID, orderData in pairs(orderIDToState) do
		if orderData.waypointID == ID then
			ordersToReset[#ordersToReset + 1] = orderData.id
		end
	end
	
	waypoints[ID] = wp
end

local function WaypointRemoved(subject, encodedMessage)
	local decodedMsg = message.Decode(encodedMessage)
	local wp = decodedMsg.waypoint
	local ID = wp:GetID()
	waypoints[ID] = nil
end

local function UpdateLockState(decodedMsg, playerID)
	if playerID == PLAYER_ID then
		if WPUI.locked then
			WPUI.lockAlpha = ALPHA_UNLOCKED
			WPUI.locked = false
		else
			WPUI.lockAlpha = ALPHA_LOCKED
			WPUI.locked = true
		end
	end
end

local function UpdateDebugState(decodedMsg, playerID)
	if playerID == PLAYER_ID then
		if WPUI.debug then
			WPUI.debug = false
		else
			WPUI.debug = true
		end
	end
end

local function TryDeleteWaypoint()
	local mouseX, mouseY, lmbPressed, middlePressed, rmbPressed, offscreen = SpringGetMouseState() 
	local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
	local mapPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
	local wp = FindWaypoint(mapPosition, WAYPOINT_SELECTION_DISTANCE)
	if wp and
		wp.ownerID == PLAYER_ID
	then
		RequestRemoveWaypoint(wp)
	end
end

local function TryChangeType()
	local mouseX, mouseY, lmbPressed, middlePressed, rmbPressed, offscreen = SpringGetMouseState() 
	local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
	local mapPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
	local wp = FindWaypoint(mapPosition, WAYPOINT_SELECTION_DISTANCE)
	if wp and
		wp.ownerID == PLAYER_ID
	then
		local currentDefID = wp:GetDefID()
		local newDefID = ((currentDefID) % #WAYPOINT_DEFS) + 1
		wp:SetDefID(newDefID)
		RequestUpdateWaypoint(wp)
	end
end

function widget:Initialize()
	-- handlers for events from gadget
	widgetHandler:RegisterGlobal('C4FullUpdate', FullUpdate)
	widgetHandler:RegisterGlobal('C4WaypointUpdated', WaypointUpdated)
	widgetHandler:RegisterGlobal('C4WaypointRemoved', WaypointRemoved)
	widgetHandler:RegisterGlobal('C4EdgeUpdated', EdgeUpdated)
	-- get all stuff
	RequestAllWaypoints()
end

function widget:RecvLuaMsg(encodedMessage, playerID)
	-- handlers for events from widget
	local decodedMsg = message.Decode(encodedMessage)
	if decodedMsg ~= nil then
		if decodedMsg.subject == 'C4UpdateLockState' then UpdateLockState(decodedMsg, playerID) end
		if decodedMsg.subject == 'C4UpdateDebugState' then UpdateDebugState(decodedMsg, playerID) end
	end
end

function widget:KeyPress(key, mods, isRepeat)
	if not isRepeat then
		if (key == KEY_SHIFT) then
			WPUI.shiftPressed = true
		end
		if (key == KEY_SPACE) then
			WPUI.spacePressed = true
		end
		if (key == KEY_CONTROL) then
			WPUI.ctrlPressed = true
		end
	end
end

function widget:KeyRelease(key)
	if (key == KEY_SHIFT) then
		WPUI.shiftPressed = false
	end
	if (key == KEY_SPACE) then
		WPUI.spacePressed = false
	end
	if (key == KEY_CONTROL) then
		WPUI.ctrlPressed = false
	end
	if (key == KEY_DELETE) then
		TryDeleteWaypoint()
	end
	if (key == KEY_Q) then
		TryChangeType()
	end
end

function widget:MousePress(mouseX, mouseY, button)
	local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
	if mapCoords ~= nil then -- is it on the map?
		local mapPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
		local wp = FindWaypoint(mapPosition, WAYPOINT_SELECTION_DISTANCE)
		if not WPUI.locked then			
			if 
				wp and -- we found some waypoint
				button == 1 and -- LMB
				WPUI.selected == nil -- and there none selected, yet
			then
				--Spring.Echo(tostring(wp),button)
				WPUI.selected = wp -- save selected waypoints
				WPUI.selectedHold = true
				return true
			end
			
			if 
				WPUI.spacePressed
			then
				local newWP = POI(
					_, -- let synced system to assign ID
					1,
					PLAYER_ID, 
					LOCAL_LAYER_ID, -- it gets global layerID in synced system
					mapPosition,
					0
				)
				RequestAddWaypoint(newWP)
			end
		end
		
		if
			button == 3 -- RMB
		then
			local units = Spring.GetSelectedUnits()
			if
				wp
			then
				-- temporary formations and giving orders
				if (#units > 0) then
					
					-- positions calculation
					local positions = {}
					local totalX, totalY, totalZ = 0, 0, 0
					local waypointPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
					for i=1, #units do
						local unitID = units[i]
						local commands = SpringGetUnitCommands(unitID)
						local x,y,z 
						if 
							WPUI.shiftPressed and
							#commands > 1
						then
							local lastCommand = commands[#commands-1]
							if 	
								lastCommand.id == CMD.FIGHT or
								lastCommand.id == CMD.MOVE or
								lastCommand.id == CMD.ATTACK
							then
								local params = lastCommand.params
								x,y,z = params[1], params[2], params[3]
							else -- fallback
								x,y,z = SpringGetUnitPosition(unitID)
							end
						else
							x,y,z = SpringGetUnitPosition(unitID)
						end
						positions[unitID] = Vec3(x,y,z)
						totalX = totalX + x
						totalY = totalY + y
						totalZ = totalZ + z
					end
					local avg = Vec3(totalX / #units, totalY / #units, totalZ / #units)
					for i=1, #units do
						local unitID = units[i]
						local relativePosition = positions[unitID] - avg
						unitsRelativePositions[unitID] = relativePosition
						unitsFinalPositions[unitID] = (waypointPosition + relativePosition):ToMap()
					end
					
					-- orders structures
					local newOrderID = 1
					for i=1, 100 do
						newOrderID = math.random(100000)
						if orderIDToState[newOrderID] == nil then
							break
						end
					end					
					if orderIDToState[newOrderID] ~= nil then
						newOrderID = #orderIDToState + 1 -- fallback
					end
					
					-- giving orders
					local keyMeta = {}
					local cmdID = CMD.FIGHT
					
					if WPUI.shiftPressed then
						keyMeta = {"shift"}
					end
					if WPUI.ctrlPressed then
						cmdID = CMD.MOVE
					end
					local unitsNotFinished = {}
					for i=1, #units do
						local unitID = units[i]
						local cmdParams = unitsFinalPositions[unitID]:AsSpringVector()
						CustomGiveOrder(unitID, cmdID, cmdParams, keyMeta, waypointPosition)
						unitIDToOrderID[unitID] = newOrderID
						unitsNotFinished[unitID] = true
					end
					orderIDToState[newOrderID] = {
						id = newOrderID,
						cmdID = cmdID,
						phase = "issued",
						remainingUnits = unitsNotFinished,
						waypointID = wp:GetID(),
						unitsCount = #units,
					}
					UpdateGroup(units)
					return true
				end
			else -- we need to make sure units are no longer tracked
				for i=1, #units do
					local unitID = units[i]
					CustomOrderCleanupFull(unitID)
				end
			end
		end
	end
end

function CustomGiveOrder(unitID, cmdID, cmdParams, keyMeta, waypointPosition)	
	if unitIDCanReclaim[unitID] == nil then
		local unitDefID = SpringGetUnitDefID(unitID)
		unitIDCanReclaim[unitID] = UnitDefs[unitDefID].canReclaim
	end
	
	if unitIDCanReclaim[unitID] then
		local distance = math.max(unitsFinalPositions[unitID]:Distance(waypointPosition), RECLAIM_DISTANCE_MIN)
		local newCmdParms = {cmdParams[1], cmdParams[2], cmdParams[3], distance}
		Spring.GiveOrderToUnit(
			unitID,
			CMD.RECLAIM,
			newCmdParms,
			keyMeta
		)
		keyMeta = {"shift"}
	end
	Spring.GiveOrderToUnit(
		unitID,
		cmdID,
		cmdParams,
		keyMeta
	)
	unitIDToNotMovingTime[unitID] = 0
end

function CustomOrderCleanupFull(unitID)
	local lastOrderID = unitIDToOrderID[unitID]
	if 
		lastOrderID and
		orderIDToState[lastOrderID] and
		orderIDToState[lastOrderID].remainingUnits
	then
		orderIDToState[lastOrderID].remainingUnits[unitID] = nil
	end
	CustomOrderCleanup(unitID)
end
function CustomOrderCleanup(unitID)	
	unitsLastPositions[unitID] = nil
	unitsFinalPositions[unitID] = nil
	unitIDToOrderID[unitID] = nil
	unitIDToNotMovingTime[unitID] = nil
	unitIDDistanceToFinalPosition[unitID] = nil	
end

function widget:GameFrame(frame)
	if frame % aiFrameLength == 0 then
		local unitsList = {}
		for unitID,_ in pairs(unitsLastPositions) do
			unitsList[#unitsList+1] = unitID
		end
		for i=1, #unitsList do
			local x,y,z = SpringGetUnitPosition(unitsList[i])
			unitsLastPositions[unitsList[i]] = Vec3(x,y,z)
		end
		aiFrameLength = math.max(15, math.floor(#unitsList / 10))
	end
	if frame % aiFrameLength == (aiFrameLength-1) then
		local ordersCompleted = {}
		for orderID, orderData in pairs(orderIDToState) do
			if orderData.phase == "issued" then
				local toRemove = {}
				local remainingUnits = orderData.remainingUnits
				local totalDistance = 0
				local unitsCount = orderData.unitsCount
				for unitID, _ in pairs(remainingUnits) do
					local isValid = SpringValidUnitID(unitID)
					if isValid then
						local x,y,z = SpringGetUnitPosition(unitID)
						local position = Vec3(x,y,z)
						local commands = SpringGetUnitCommands(unitID,1)
						if (unitsLastPositions[unitID] == nil) then
							local lx,ly,lz = SpringGetUnitPosition(unitID)
							unitsLastPositions[unitID] = Vec3( lx,ly,lz)
						end
						local distanceToLastPosition = position:Distance(unitsLastPositions[unitID])
						local distanceToFinalPosition = position:Distance(unitsFinalPositions[unitID])
						unitIDDistanceToFinalPosition[unitID] = distanceToFinalPosition
						totalDistance = totalDistance + distanceToFinalPosition
						if distanceToFinalPosition < WAYPOINT_REACHED_RADIUS then
							toRemove[#toRemove + 1] = unitID
						else
							if distanceToLastPosition < MOVE_COMPLETE_THRESHOLD_SMALL + 1 then
								unitIDToNotMovingTime[unitID] = (unitIDToNotMovingTime[unitID] or 0) + 1
							else
								unitIDToNotMovingTime[unitID] = 0
							end
							
							-- attempt to unstuck
							if 
								#commands == 0 and
								distanceToFinalPosition > 5*WAYPOINT_REACHED_RADIUS and
								unitIDToNotMovingTime[unitID] > 0
							then
								local wpPosition = waypoints[orderData.waypointID]:GetPosition()
								local cmdParams = unitsFinalPositions[unitID]:AsSpringVector()
								CustomGiveOrder(
									unitID, 
									CMD.MOVE, 
									{x + (-50 + math.random(100)),y,z + (-50 + math.random(100))}, 
									{},
									wpPosition
								)
								CustomGiveOrder(
									unitID, 
									orderData.cmdID, 
									cmdParams, 
									{"shift"},
									wpPosition
								)
							end
						end
					else
						toRemove[#toRemove + 1] = unitID
						CustomOrderCleanup(unitID)
					end
				end
				
				for i=1, #toRemove do
					local unitID = toRemove[i]
					orderIDToState[orderID].remainingUnits[unitID] = nil					
				end
				local counter = 0
				local timoutSuccess = false
				for unitID,_ in pairs(orderData.remainingUnits) do
					counter = counter + 1
					if (unitIDToNotMovingTime[unitID] > (totalDistance/unitsCount)) then
						timoutSuccess = true
					end
				end	
				
				--Spring.Echo(counter)
				if counter == 0  or timoutSuccess then
					orderIDToState[orderID].phase = "completed"
					ordersCompleted[#ordersCompleted + 1] = orderData.id
				end
			elseif orderData.phase == "completed" then
				ordersCompleted[#ordersCompleted + 1] = orderData.id
			elseif orderData.phase == "reset" then
				ordersToReset[#ordersToReset + 1] = orderData.id
			end
		end
		
		for i=1, #ordersCompleted do
			local orderID = ordersCompleted[i]
			local orderData = orderIDToState[orderID]
			local cmdID = orderIDToState[orderID].cmdID
			orderIDToState[orderID].phase = "completed"
			
			local wp = waypoints[orderData.waypointID]
			if wp then
				local edgesOut = wp:GetEdgesOut()
				local chosenID
				for wpID, edgeData in pairs(edgesOut) do
					chosenID = wpID
					if math.random() < 0.5 then
						break
					end
				end
				
				-- Spring.Echo("phase completed, next: ", chosenID)
				local chosenWP = waypoints[chosenID]
				
				if chosenWP then
					orderIDToState[orderID] = {
						id = orderID,
						cmdID = cmdID,
						phase = "reset",
						waypointID = chosenWP:GetID()
					}
				end
			else
				orderIDToState[orderID] = nil
			end
		end
		
		for i=1, #ordersToReset do
			local orderID = ordersToReset[i]
			local orderData = orderIDToState[orderID]
			local cmdID = orderData.cmdID
			local wp = waypoints[orderData.waypointID]
			if wp then
				orderIDToState[orderID] = {
					id = orderID,
					cmdID = cmdID,
					phase = "issued",
					waypointID = wp:GetID()
				}
				local unitsNotFinished = {}
				local wpPosition = wp:GetPosition()
				local unitsCount = 0
				for unitID, unitOrderID in pairs(unitIDToOrderID) do
					if (orderID == unitOrderID) then					
						local finalPosition = (wpPosition + unitsRelativePositions[unitID]):ToMap()
						unitsFinalPositions[unitID] = finalPosition
						local cmdParams = finalPosition:AsSpringVector()
						CustomGiveOrder(
							unitID, 
							cmdID, 
							cmdParams, 
							{},
							wpPosition
						)
						unitIDToOrderID[unitID] = orderID
						unitsNotFinished[unitID] = true
						unitsCount = unitsCount + 1
					end
				end
				orderIDToState[orderID].remainingUnits = unitsNotFinished
				orderIDToState[orderID].unitsCount = unitsCount
				
				if unitsCount == 0 then -- all units destroyed, remove order
					orderIDToState[orderID] = nil
				end
			end
		end
		ordersToReset = {}
	end	
end

function widget:MouseRelease(mouseX, mouseY, button)
	if not WPUI.locked then
		local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
		if mapCoords ~= nil then -- is it on the map?
			local mapPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
			local wp = FindWaypoint(mapPosition, WAYPOINT_SELECTION_DISTANCE)
			if 
				button == 1 -- LMB
			then
				if 
					wp and -- we found some waypoint
					WPUI.selected ~= nil and -- and other point selected
					WPUI.selected ~= wp -- not connecting to self
				then -- create/remove the edge between two points
					if (not (WPUI.selected > wp)) then
						RequestAddEdge(WPUI.selected, wp, 1)
						WPUI.selected = WPUI.selected + wp -- "connect" method (for local feedback)
					else
						RequestRemoveEdge(WPUI.selected, wp)
						WPUI.selected = WPUI.selected - wp -- "disconnect" method (for local feedback)
					end
				elseif 
					WPUI.creationPosition
				then -- make new waypoint			
					local previousWPPosition = WPUI.selected:GetPosition()
					local newWP = POI(
						_, -- let synced system to assign ID
						1,
						PLAYER_ID, 
						LOCAL_LAYER_ID, -- it gets global layerID in synced system
						mapPosition,
						(mapPosition - previousWPPosition):ToHeading()
					)
					RequestAddEdge(WPUI.selected, newWP, 1)
				elseif 
					wp
				then -- some at least position manipulation is probable
					RequestUpdateWaypoint(wp)
				end		
			end
		end
		
		-- no matter what get out of connection/creation mode
		WPUI.creationPosition = nil
	end
end

function widget:Update(dt)
	if not WPUI.locked then
		local mouseX, mouseY, lmbPressed, middlePressed, rmbPressed, offscreen = SpringGetMouseState() 
		local desc, mapCoords = SpringTraceScreenRay(mouseX, mouseY, true) -- onlyCoords only
		if mapCoords ~= nil then -- is it on the map?
			
			-- waypoint deselection
			if not lmbPressed and WPUI.selected then
				WPUI.selected = nil
				WPUI.selectedHold = false
			end		
			
			if (not WPUI.shiftPressed) then
				-- if not creating new waypoint
				if WPUI.creationPosition == nil then
					-- waypoint drag & drop to change position
					if 
						lmbPressed and 
						WPUI.selectedHold and 
						WPUI.selected:GetOwnerID() == PLAYER_ID and
						mapCoords ~= nil
					then
						WPUI.selected:SetPosition(Vec3(mapCoords[1], mapCoords[2], mapCoords[3]))
						-- we update the position only locally until the move is finished
						-- RequestUpdateWaypoint(WPUI.selected)
					end
				else
					-- update position of new waypoint
					if 
						lmbPressed and 
						WPUI.selectedHold 
					then
						WPUI.creationPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
					end
				end
			end
			
			if (WPUI.shiftPressed) then
				-- trigger adding new waypoint on top of existing
				if lmbPressed and WPUI.selectedHold then
					WPUI.creationPosition = Vec3(mapCoords[1], mapCoords[2], mapCoords[3])
				end
			end	
		end
	end
end

function widget:Shutdown()
	--Save()
end

-- drawing part
function widget:DrawWorld()
	local function DrawOwner(ownerName, x, y, z )
		glPushMatrix()
		glTranslate(x + WAYPOINT_OWNER_DISTANCE, y, z)
		glBillboard()		
		fontOwner:Print(ownerName, 0, 0)
		glPopMatrix()		
	end
	local function DrawDebug(waypointData, x, y, z)
		glPushMatrix()
		glTranslate(x + WAYPOINT_OWNER_DISTANCE, y, z + WAYPOINT_DEBUG_LINE)
		glBillboard()
		local counter = 0
		for key,value in pairs(waypointData) do
			local valueToPrint = tostring(value)
			fontDebug:Print(fontDebugGrey .. key .. ": " .. valueToPrint .. fontColorReset, 0, -counter*WAYPOINT_DEBUG_LINE)
			counter = counter + 1
		end
		glPopMatrix()		
	end
	
	glLineWidth(1) -- to make sure its not overriden by other widget
	for ID, wp in pairs(waypoints) do
		local waypointPosition = wp:GetPosition()
		local wpx, wpy, wpz = waypointPosition:GetCoordinates()
		
		-- waypoint drawing based on type
		local wpDefID = wp:GetDefID()
		local wpDrawFunction = WAYPOINT_DEFS[wpDefID].drawFunction
		wpDrawFunction(wp, wpx, wpy, wpz)

		-- ownership
		local ownerID = wp:GetOwnerID()
		if ownerID == PLAYER_ID then
			WAYPOINTS_COLORS[WAYPOINT_COLORS_MINE]()
		else
			WAYPOINTS_COLORS[WAYPOINT_COLORS_NOT_MINE]()
		end
		if WPUI.debug then
			DrawDebug(wp, wpx, wpy, wpz)
		end
		
		glDrawGroundCircle(wpx, wpy, wpz, WAYPOINT_SIZE+5, CIRCLE_SEGMENTS)
		local ownerName = SpringGetPlayerInfo(ownerID) or "internal"
		DrawOwner(ownerName, wpx, wpy, wpz)		
		
		for outID, edgeDefID in pairs(wp.edgesOut) do
			local outPosition = waypoints[outID]:GetPosition()
			local directionNormal = (outPosition - waypointPosition):Normalize()
			local newOutPosition = outPosition - (directionNormal * WAYPOINT_SIZE)
			local arrowPosition = outPosition - (directionNormal * WAYPOINT_ARROW_LENGTH):RotateByHeading(30)
			local outx, outy, outz = newOutPosition:GetCoordinates()
			local arrowx, arrowy, arrowz = arrowPosition:GetCoordinates()
			EDGES_COLORS[edgeDefID]()
			glBeginEnd(GL.LINES,
				function()
					glVertex(wpx, wpy, wpz)
					glVertex(outx, outy, outz)
				end
			)
			glBeginEnd(GL.LINES,
				function()
					glVertex(arrowx, arrowy, arrowz)
					glVertex(outx, outy, outz)
				end
			)
		end
	end
	
	if WPUI.selected and WPUI.creationPosition ~= nil then
		local wpx, wpy, wpz = WPUI.creationPosition:GetCoordinates()
		WAYPOINTS_COLORS[WAYPOINT_COLORS_NEW]()
		glDrawGroundCircle(wpx, wpy, wpz, WAYPOINT_SIZE, CIRCLE_SEGMENTS)
		
		local selx, sely, selz = WPUI.selected:GetPositionCoordinates()
		local pattern = (65536 - 775)
		local patternShift = math.floor(math.fmod(SpringGetGameSeconds() * 16, 16))
		EDGES_COLORS[EDGES_COLORS_NEW]()
		glLineStipple(2, pattern, patternShift)		
		glBeginEnd(GL.LINES,
			function()
				glVertex(wpx, wpy, wpz)
				glVertex(selx, sely, selz)
			end
		)
		glLineStipple(false)
	end
	
	glLineWidth(1)
	glColor(1, 1, 1, 1)
end