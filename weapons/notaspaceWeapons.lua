-- temp all-in-one-file defs

VFS.Include("gamedata/configs/constants.lua")

local notAspaceWeaponsDefs = {
	rifle = {
		name = "Laser rifle",
		weaponType = RIFLE_BASE.weaponType,
		range = RIFLE_BASE.range,
		reloadTime = RIFLE_BASE.reloadTime,
		weaponVelocity = RIFLE_BASE.weaponVelocity,
		duration = 0.016,
		burst = RIFLE_BASE.burst,
		firestarter = 8,
		
		-- fireTolerance = 5000,
		accuracy = 25,
		turret = true, -- fire when weapon aim, not sooner
		
		avoidFriendly = false,
		collideFriendly = false,
		avoidFeature = false,
		collideFeature = true,
		
		damage = {
			default = 125,
		},
		
		thickness = 3.0,
		coreThickness = 0.3,
		laserFlareSize = 8,
		rgbColor = RIFLE_BASE.rgbColor,
		rgbColor2 = RIFLE_BASE.rgbColor2,
		
		explosionGenerator = "custom:FLASH1red",
		soundStart = "lasrfir1",
		soundHit = "lasrhit2",
		soundTrigger = true,
	},
	
	machinegun = {
		name = "Machine gun",
		weaponType = MG.weaponType,
		range = MG.range,
		reloadTime = MG.reloadTime,
		weaponVelocity = MG.weaponVelocity,
		duration = 0.2,
		burst = MG.burst,
		burstrate =  MG.burstrate,
		firestarter = 8,
		
		-- fireTolerance = 5000,
		accuracy = MG.accuracy,
		sprayangle = MG.sprayangle,
		--targetMoveError = MG.targetMoveError,
		turret = true, -- fire when weapon aim, not sooner
		
		avoidFriendly = false,
		collideFriendly = false,
		avoidFeature = false,
		collideFeature = true,
		
		damage = {
			default = 200,
		},
		paralyzer = true,
		paralyzeTime = 2,
		areaofeffect = MG.areaofeffect,
		
		size = 4,
		-- thickness = 8.0,
		-- coreThickness = 4,
		-- laserFlareSize = 18,
		stages = 10, -- armwar
		sizeDecay = -0.1, -- armwar 
		alphaDecay = 0.5, -- armwar
		rgbColor = MG.rgbColor,
		rgbColor2 = MG.rgbColor2,
		
		explosionGenerator = "custom:blueEMGhit",
		soundTrigger = false,
		soundStart = "armsml2",
		soundHit = "lasrhit1",
		soundTrigger = true,
		
	},
}

return lowerkeys(notAspaceWeaponsDefs)