local moduleInfo = {
	name = "basicObjects",
	desc = "Objects taken from UnitDefs", 
	author = "PepeAmpere",
	date = "2019-01-17",
	license = "MIT",
}

newSpawnObjects = {}

-- create defs for all 
for id, unitDef in pairs(UnitDefs) do
	newSpawnObjects[unitDef.name] = {
		name = unitDef.name,
		unit = unitDef.name, -- legacy mission spawner support
	}
end

-- END OF MODULE DEFINITIONS --

-- update global tables 
if (spawnObjects == nil) then spawnObjects = {} end
for k,v in pairs(newSpawnObjects) do
	if (spawnObjects[k] ~= nil) then Spring.Echo("NOTIFICATION: Attempt to rewrite global table in module [" .. moduleInfo.name ..  "] - key: " .. k) end
	spawnObjects[k] = v 
end