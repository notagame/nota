local moduleInfo = {
	name = "basicProjectiles",
	desc = "Projectils taken from WeaponsDefs", 
	author = "PepeAmpere",
	date = "2019-01-17",
	license = "MIT",
}

-- projectile defs

newSpawnProjectiles = {
}

-- END OF MODULE DEFINITIONS --

-- update global tables 
if (spawnProjectiles == nil) then spawnProjectiles = {} end
for k,v in pairs(newSpawnProjectiles) do
	if (spawnProjectiles[k] ~= nil) then Spring.Echo("NOTIFICATION: Attempt to rewrite global table in module [" .. moduleInfo.name ..  "] - key: " .. k) end
	spawnProjectiles[k] = v 
end

