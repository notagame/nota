local moduleInfo = {
	name = "notaLegacyObjects",
	desc = "Objects from legacy defs", 
	author = "PepeAmpere",
	date = "2019-01-17",
	license = "MIT",
}

-- objects defs
--[[
	-- name: unitDef.name
	-- events: list of events for which the spawn is registered
	-- delay: spawn time delay wanted, default 0
	-- expiration: time after which unit is removed from game, default nil (never)
	-- ownerTeamID: creator teamID
	-- creatorUnitID: creator unitID
	-- ownerNeutralOverride: unit spawned as gaia for transition units, default false => using owner team
	-- SpawnDefFunction: specific function for manipulation with the defintion (getting full object as param), default none
]]--

local HMSF = attach.Module(modules, "hmsf")
local Vec3 = attach.Module(modules, "vec3")

newSpawnObjects = {
	["corbuzz_ak"] = { 
		name = "corak",
		events = {
			explosion = {name = "corbuzz_ak"},
		},
	},
	["armvulc_pw"] = { 
		name = "armpw",
		events = {
			explosion = {name = "armvulc_pw"},
		},
	},
	["cor_supergun_pyro"] = { 
		name = "corpyro",
		events = {
			explosion = {name = "cor_supergun_pyro"},
		},
	},
	["cortron_weapon"] = { 
		name = "scramerunit",
		events = {
			explosion = {name = "cortron_weapon"},
		},
		expiration = HMSF(0,0,30,0),
		ownerNeutralOverride = true,
		SpawnDefFunction = function(spawnDef, explosionData)
			spawnDef.position = explosionData.position + Vec3(0, 1000, 0)
			return spawnDef
		end,
	},
	["armemp_weapon"] = { 
		name = "emperunit",
		events = {
			explosion = {name = "armemp_weapon"},
		},		
		expiration = HMSF(0,0,30,0),
		ownerNeutralOverride = true,
		SpawnDefFunction = function(spawnDef, explosionData)
			spawnDef.position = explosionData.position + Vec3(0, 600, 0)
			return spawnDef
		end,
	},
	["arm_empmine"] = { 
		name = "emperunit",
		events = {
			explosion = {name = "arm_empmine"},
		},
		expiration = HMSF(0,0,25,0),
		ownerNeutralOverride = true,
		SpawnDefFunction = function(spawnDef, explosionData)
			spawnDef.position = explosionData.position + Vec3(0, 600, 0)
			return spawnDef
		end,
	},
	["high_bugasteroidbeacon"] = { 
		name = "spawner_rockstorm",
		events = {
			explosion = {name = "high_bugasteroidbeacon"},
		},
		expiration = HMSF(0,0,10,0),
		ownerNeutralOverride = false,
		SpawnDefFunction = function(spawnDef, explosionData)
			spawnDef.position = explosionData.position + Vec3(0, 250, 0)
			return spawnDef
		end,
	},
	["darkswarm"] = { 
		name = "darkswarmunit",
		events = {
			explosion = {name = "darkswarm"},
		},
		expiration = HMSF(0,0,14,0),
		ownerNeutralOverride = false,
		SpawnDefFunction = function(spawnDef, explosionData)
			spawnDef.position = explosionData.position + Vec3(0, 100, 0)
			return spawnDef
		end,
	},
}


-- END OF MODULE DEFINITIONS --

-- update global tables 
if (spawnObjects == nil) then spawnObjects = {} end
for k,v in pairs(newSpawnObjects) do
	if (spawnObjects[k] ~= nil) then Spring.Echo("NOTIFICATION: Attempt to rewrite global table in module [" .. moduleInfo.name ..  "] - key: " .. k) end
	spawnObjects[k] = v 
end