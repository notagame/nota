local MODULE_NAME = "spawn"
Spring.Echo("-- " .. MODULE_NAME .. " LOADING --")

------------------------------------------------------

-- MANDATORY
-- check required modules
if (modules == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory config [modules] listing paths for modules is missing") end
if (attach == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory library [attach] for loading files and modules is missing") end

-- there has to exist modules table which is included before init is called
local thisModuleData = modules[MODULE_NAME]
local THIS_MODULE_DATA_PATH = thisModuleData.data.path

------------------------------------------------------

-- OPTIONAL CONFIG LOAD 
local THIS_MODULE_CONFIG_PATH = thisModuleData.config.path -- custom configs folder (optional)
local listOfFiles = thisModuleData.config.files -- list of files in configPath
attach.try.ModuleOptionalConfigs(THIS_MODULE_CONFIG_PATH, listOfFiles)

------------------------------------------------------

-- LOAD INTERNAL MODULE FUNCTIONALITY
--attach.File(THIS_MODULE_DATA_PATH .. "api/messageSender.lua") -- API for sending messages

------------------------------------------------------

Spring.Echo("-- " .. MODULE_NAME .. " LOADING FINISHED --")

